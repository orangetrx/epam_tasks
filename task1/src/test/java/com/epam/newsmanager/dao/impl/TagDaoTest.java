package com.epam.newsmanager.dao.impl;

import com.epam.newsmanager.bean.Tag;
import com.epam.newsmanager.dao.TagDao;
import com.epam.newsmanager.exception.DaoException;
import com.epam.newsmanager.util.DbTestUtil;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.DatabaseTearDowns;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import javax.sql.DataSource;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:test-app-contex.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
                            DirtiesContextTestExecutionListener.class,
                            TransactionalTestExecutionListener.class,
                            DbUnitTestExecutionListener.class })
@DatabaseSetup(value = {"classpath:com/epam/newsmanager/dao/NewsDaoTest.xml",
        "classpath:com/epam/newsmanager/dao/TagDaoTest.xml",
        "classpath:com/epam/newsmanager/dao/news_tags_dataset.xml"})
@DatabaseTearDowns(
        {
                @DatabaseTearDown(value = "classpath:com/epam/newsmanager/dao/news_tags_dataset.xml", type = DatabaseOperation.DELETE_ALL),
                @DatabaseTearDown(value={"classpath:com/epam/newsmanager/dao/TagDaoTest.xml"}, type = DatabaseOperation.DELETE_ALL)
        }
)
public class TagDaoTest {

    @Autowired
    private TagDao tagDao;

    @Autowired
    private DataSource dataSource;

    @After
    public void reset() {
        DbTestUtil.resetAutoIncrementInTags(dataSource);
        DbTestUtil.resetAutoIncrementInNews(dataSource);
    }

    @Test
    public void testRead() throws DaoException {
        Tag dummyTag;
        long dummyTagId = 1;

        dummyTag = tagDao.read(dummyTagId);
        Assert.assertEquals(dummyTagId, dummyTag.getTagId());
        Assert.assertEquals(dummyTag.getTagName(), "tag1");
    }

    @Test
    public void testReadByName() throws DaoException {
        Tag dummyTag;
        String dummyTagName = "tag2";
        dummyTag = tagDao.readByName(dummyTagName);
        Assert.assertEquals(dummyTag.getTagName(), dummyTagName);
        Assert.assertEquals(dummyTag.getTagId(), 2);
    }


    @Test
    public void testCreate() throws DaoException {
        Tag dummyTag = new Tag();
        dummyTag.setTagName("tagg");
        long tagId = tagDao.create(dummyTag);
        Tag actualTag = tagDao.read(tagId);
        Assert.assertEquals(actualTag.getTagName(), dummyTag.getTagName());
    }

    @Test
    public void testGetAllTags() throws DaoException {
        int expectedListSize = 3;
        List<Tag> dummyTagList;
        dummyTagList = tagDao.getAllTags();
        Assert.assertEquals(expectedListSize, dummyTagList.size());
    }

    @Test
    public void testGetTagsForNews() throws DaoException {
        int expectedCountTags = 2;
        List<Tag> dummyTagList;
        dummyTagList = tagDao.getTagsForNews(new Long(1));
        Assert.assertEquals(expectedCountTags, dummyTagList.size());
    }
}
