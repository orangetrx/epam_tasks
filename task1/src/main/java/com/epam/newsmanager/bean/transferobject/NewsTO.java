package com.epam.newsmanager.bean.transferobject;

import com.epam.newsmanager.bean.Author;
import com.epam.newsmanager.bean.Comment;
import com.epam.newsmanager.bean.News;
import com.epam.newsmanager.bean.Tag;

import java.io.Serializable;
import java.util.List;

/**
 * NewsTO is a class that containing NEWS object with it set of
 * TAGS and COMMENTS and AUTHOR of NEWS;
 */
public class NewsTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private News news;

    private Author author;
    private List<Tag> tags;
    private List<Comment> comments;

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

}
