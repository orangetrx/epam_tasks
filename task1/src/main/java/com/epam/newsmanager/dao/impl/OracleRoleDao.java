package com.epam.newsmanager.dao.impl;

import com.epam.newsmanager.bean.Role;
import com.epam.newsmanager.dao.RoleDao;
import com.epam.newsmanager.exception.DaoException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * OracleAuthorDao class is a specific realization of
 * {@link RoleDao} interface. According to this
 * class you can interact with database through simple JDBC. Also it's using
 * injection of dataSource through Spring.
 */
@Component
@Scope("singleton")
public class OracleRoleDao implements RoleDao {

    private static final Logger LOGGER = Logger.getLogger(OracleRoleDao.class.getPackage().getName());

    private static final String SQL_INSERT_ROLE_QUERY = "insert into \"ROLES\" (ROL_USER_ID, ROL_NAME) values (?, ?)" ;
    private static final String SQL_SELECT_ROLE_QUERY = "select ROL_ID, ROL_NAME, ROL_USER_ID from \"ROLES\" where ROL_ID = ?";
    private static final String SQL_SELECT_ROLES_BY_USER_ID = "select ROL_ID, ROL_NAME, ROL_USER_ID from \"ROLES\" where ROL_USER_ID = ?";
    private static final String SQL_DELETE_ROLES_BY_USER_ID = "delete from \"ROLES\" where ROL_USER_ID = ?";

    /**
     * Some datasource that already configured for concrete
     *            database(e.g for oracle url/pass/login/driver).
     *            Annotation @Autowired is using for spring DI of DataSource.
     *            Configuration for oracle database you can see in
     *            database.properties file.
     */
    @Autowired
    private DataSource dataSource;

    /**
     * Implementation of
     * {@link RoleDao#create(Object)} method through
     * simple JDBC.
     */
    @Override
    public Long create(Role newInstance) throws DaoException {

        Long idRole = null;
        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT_ROLE_QUERY, new String[]{"ROL_ID"})) {
            statement.setLong(1, newInstance.getUserId());
            statement.setString(2, newInstance.getRoleName());
            statement.execute();
            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    idRole = resultSet.getLong(1);
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Insert role error");
            throw new DaoException("Error insert into db", e);
        }

        return idRole;
    }

    /**
     * Implementation of
     * {@link RoleDao#read(Serializable)} method through
     * simple JDBC.
     */
    @Override
    public Role read(Long id) throws DaoException {
        Role role = null;

        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ROLE_QUERY)) {

            statement.setLong(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    role = new Role();
                    role.setRoleId(resultSet.getLong(1));
                    role.setUserId(resultSet.getLong(3));
                    role.setRoleName(resultSet.getString(2));
                }
            }

        }  catch (SQLException e) {
            LOGGER.error("Select role error");
            throw new DaoException("Error select from db", e);
        }

        return role;
    }

    /**
     * Implementation of
     * {@link RoleDao#update(Object)} method through
     * simple JDBC.
     * In this case operation isn't available
     */
    @Override
    public void update(Role updatingInstance) throws DaoException {
        throw new UnsupportedOperationException("Role updating is unsupported");
    }

    /**
     * Implementation of
     * {@link RoleDao#delete(Serializable)} method through
     * simple JDBC.
     * In this case operation isn't available
     */
    @Override
    public void delete(Long id) throws DaoException {
        throw new UnsupportedOperationException("Role deleting is unsupported");
    }

    /**
     * Implementation of
     * {@link RoleDao#getRolesByUserId(Long)} method through
     * simple JDBC.
     */
    @Override
    public List<Role> getRolesByUserId(Long userId) throws DaoException {
        List<Role> roles = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ROLES_BY_USER_ID)) {
            statement.setLong(1, userId);
            try(ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Role role = new Role();
                    role.setRoleId(resultSet.getLong(1));
                    role.setUserId(resultSet.getLong(1));
                    role.setRoleName(resultSet.getString(3));

                    roles.add(role);
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Select roles for user error");
            throw new DaoException("Error select from db", e);
        }

        return roles;
    }

    /**
     * Implementation of
     * {@link RoleDao#deleteByUserId(Long)} method through
     * simple JDBC.
     */
    @Override
    public void deleteByUserId(Long userId) throws DaoException {

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_DELETE_ROLES_BY_USER_ID)) {
            statement.setLong(1, userId);
            statement.execute();
        } catch (SQLException e) {
            LOGGER.error("Delete user's roles error");
            throw new DaoException("Error delete from db", e);
        }
    }
}
