package com.epam.newsmanager.dao;

import com.epam.newsmanager.bean.News;
import com.epam.newsmanager.bean.transferobject.SearchCriteria;
import com.epam.newsmanager.exception.DaoException;

import java.util.List;

/**
 * NewsDao is a common interface for interactions with tables
 * NEWS,NEWS_AUTHORS,NEWS_TAGS in database.In order to interact with this tables,
 * implement this interface and write you realization for particular database;
 * This interface extends by generic interface that contains C.R.U.D. operations {@link GenericDao}
 */
public interface NewsDao extends GenericDao<News, Long> {
    /**
     * /**
     * This method is using fore loading all news that are in database.ONE
     * REMARK that list of news will be sorted by the number of comments and
     * modification date.
     *
     * @return list of all news. Maybe empty if database is empty
     * @throws DaoException
     *             this exception throws when on dao layer (e.g.) SQLException
     *             caught.
     */
    List<News> getAllNews() throws DaoException;

    /**
     * This method return count of all news that are in database
     * @return number of all news
     * @throws DaoException
     *              this exception throws when on dao layer (e.g.) SQLException
     *              caught.
     */
    int getCountOfAllNews() throws DaoException;

    /**
     * This method is using for creating new linking record in NEWS_AUTHOR
     * table.
     *
     * @param idNews
     *            ID of NEWS object to connect.
     * @param idAuthor
     *            ID of AUTHOR object to connect.
     * @throws DaoException
     *             this exception throws when on dao layer (e.g.) SQLException
     *             caught.
     */
    boolean insertNewsAuthorLink(Long idAuthor, Long idNews) throws DaoException;

    /**
     * This method is using for creating new linking record in NEWS_TAG table.
     *
     * @param idNews
     *            ID of NEWS object to connect.
     * @param idTag
     *            ID of TAG object to connect.
     * @throws DaoException
     *             this exception throws when on dao layer (e.g.) SQLException
     *             caught.
     */
    boolean insertNewsTagLink(Long idNews, Long idTag) throws DaoException;

    /**
     * This method is using for deleting linking record from NEWS_AUTHOR table.
     *
     * @param idNews
     *            ID of NEWS object to delete from linking table.
     * @throws DaoException
     *             this exception throws when on dao layer (e.g.) SQLException
     *             caught.
     */
    boolean deleteNewsAuthorLink(Long idNews) throws DaoException;

    /**
     * This method is using for deleting linking record from NEWS_TAG table.
     *
     * @param idNews
     *            ID of NEWS object to delete from linking table.
     * @throws DaoException
     *             this exception throws when on dao layer (e.g.) SQLException
     *             caught.
     */
    boolean deleteNewsTagLink(Long idNews) throws DaoException;

    /**
     * This method is using for loading list of News by tag (TG_ID)
     * @param tagId ID of Tag with whom news is connected.
     * @return list of found news
     * @throws DaoException
     *              this exception throws when on dao layer (e.g.) SQLException
     *              caught.
     */
    List<News> getNewsByTag(Long tagId) throws DaoException;

    /**
     * This method is using for searching news by SearchCriteria. Full
     * description of building query for searching you can find in QueryBuilder
     * class; NOTE: if buildQueryFromSearchCriteria method return empty
     * String(cause of both parameters in SearchCriteria object is null or
     * missing), result list of news will contain all news;
     *
     * @param searchCriteria
     *            {@link SearchCriteria} object
     *            that must contain all filled fields(for more info look method
     *            description);
     * @return list of news that satisfactory by passed parameters(for other
     *         cases look method description)
     * @throws DaoException
     *             this exception throws when on dao layer (e.g.) SQLException
     *             caught.
     */
    List<News> getNewsBySearchCriteria(SearchCriteria searchCriteria) throws DaoException;

}
