package com.epam.newsmanager.dao;

import com.epam.newsmanager.bean.Role;
import com.epam.newsmanager.exception.DaoException;

import java.util.List;

/**
 * RoleDao is a common interface for interactions with table ROLES in database.
 * In order to interact with this tables,
 * implement this interface and write you realization for particular database;
 * This interface extends by generic interface that contains C.R.U.D. operations {@link GenericDao}
 */
public interface RoleDao extends GenericDao<Role, Long> {

    /**
     * This method is using for loading list of Roles for user from ROLES table
     * @param userId ID of user
     * @return list that contains Role objects {@link Role}
     * @throws DaoException
     *             this exception throws when on dao layer (e.g.) SQLException
     *             caught.
     */
    List<Role> getRolesByUserId(Long userId) throws DaoException;

    /**
     * This method is using for deleting Roles by user id from ROLES table
     * @param userId ID of user
     * @throws DaoException
     */
    void deleteByUserId(Long userId) throws DaoException;
}
