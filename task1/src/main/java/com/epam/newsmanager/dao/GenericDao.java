package com.epam.newsmanager.dao;

import com.epam.newsmanager.exception.DaoException;

import java.io.Serializable;

/**
 *Interface for a Data Access Object that can be used for a single specified type entity object.
 * A single instance implementing this interface can be used only for the type of bean object specified in the type parameters.
 * Define CRUD operations
 * @param <T> The type of the entity object for which this instance is to be used.
 * @param <K> The type of the transfer object
 */
public interface GenericDao<T,K extends Serializable> {

    /**
     * Add entity to database
     * @param newInstance  added entity
     * @return transfer object for entity
     * @throws DaoException if database error was detected
     */
    K create (T newInstance) throws DaoException;

    /**
     * Get the entity with the specified type and id from the datastore.
     * @param id id of found entity
     * @return special type of entity
     * @throws DaoException if database error was detected
     */
    T read(K id) throws DaoException;

    /**
     * Update the specified entity in the database.
     * @param updatingInstance updated entity
     * @throws DaoException if database error was detected
     */
    void update(T updatingInstance) throws DaoException;

    /**
     * Remove the specified entity from the database.
     * @param id identifier of removing entity
     * @throws DaoException if database error was detected
     */
    void delete(K id) throws DaoException;


}
