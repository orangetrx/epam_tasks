package com.epam.newsmanager.exception;

import com.epam.newsmanager.exception.ApplicationException;

/**
 *
 * All exceptions that may arise in SERVICE layer, are wrapped by this custom
 * exception.
 *
 */
public class ServiceException extends ApplicationException {
    public ServiceException(String message, Exception e) {
        super(message, e);
    }

    public ServiceException(String message) {
        super(message);
    }
}
