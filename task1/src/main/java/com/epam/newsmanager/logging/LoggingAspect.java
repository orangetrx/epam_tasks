package com.epam.newsmanager.logging;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
public class LoggingAspect {

    private static final Logger LOGGER = Logger.getLogger(LoggingAspect.class.getPackage().getName());

    @AfterThrowing(pointcut = "execution (* *(..)) && within(com.epam.newsmanager.service.impl.*)", throwing = "e")
    public void logAfterThrowing(JoinPoint joinPoint, Throwable e) {
        Signature signature = joinPoint.getSignature();
        String methodName = signature.getName();
        String arguments = Arrays.toString(joinPoint.getArgs());

        LOGGER.info("caught exception in method: "
                + methodName + " with arguments "
                + arguments + "\nthe exception is: "
                + e.getMessage(), e);
    }

}
