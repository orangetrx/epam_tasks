package com.epam.newsmanager.service;

import com.epam.newsmanager.bean.News;
import com.epam.newsmanager.bean.transferobject.NewsTO;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;

import java.util.List;

public interface NewsManagementService {

    /**
     * This method is using for loading News transfer object from database by news
     * @param newsId ID of news
     * @return NewsTO object {@link NewsTO}
     * @throws ServiceException this exception throws when on service layer (e.g.)
     *             DaoException caught.
     */
    NewsTO viewNewsTO(Long newsId) throws ServiceException, NoSuchEntityException;

    /**
     * This method is using for adding news into database.
     * Also for creating links {@link NewsService#addNews(News)}.
     * @param newsTO NewsTo {@link NewsTO}
     * @return ID of created news
     * @throws ServiceException
     */
    Long addNewsTO(NewsTO newsTO) throws ServiceException, NoSuchEntityException;

    /**
     * This method is using for deleting news, news' comments and links
     * between author, tags, comments and news from database by news id
     * @param newsId deleting news' ID
     * @throws ServiceException this exception throws when on service layer (e.g.)
     *             DaoException caught.
     */
    void deleteNews(Long newsId) throws ServiceException;

    /**
     * This method is using for loading all news with tags, comments and author
     * from database
     * @return list of NewsTO {@link NewsTO}
     * @throws ServiceException this exception throws when on service layer (e.g.)
     *             DaoException caught.
     */
    List<NewsTO> viewAllNewsTO() throws ServiceException, NoSuchEntityException;

}
