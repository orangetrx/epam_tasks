package com.epam.newsmanager.service.impl;

import com.epam.newsmanager.bean.Author;
import com.epam.newsmanager.dao.AuthorDao;
import com.epam.newsmanager.exception.DaoException;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
import com.epam.newsmanager.service.AuthorService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * AuthorServiceImpl is an implementation of
 * {@link AuthorService} interface. At this
 * point this class are injected by
 * {@link AuthorDao} object via Spring. So at now it
 * have the same functionality. But also it can be expanded by another
 * functional methods.
 */
@Service("authorService")
@Transactional(rollbackFor = ServiceException.class)
public class AuthorServiceImpl implements AuthorService {


    @Inject
    @Named("oracleAuthorDao")
    private AuthorDao authorDao;

    /**
     * Implementation of
     * {@link AuthorService#addAuthor(Author)} method
     * This method has the same functionally as
     * {@link AuthorDao#create(Object)}
     */
    @Override
    public Long addAuthor(Author author) throws ServiceException, NoSuchEntityException {
        Long idAuthor;

        try {
            idAuthor = authorDao.create(author);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }

        if (idAuthor == null) {
            throw new NoSuchEntityException("Author wasn't added");
        }

        return idAuthor;
    }

    /**
     * Implementation of
     * {@link AuthorService#loadAuthor(long)} method
     * This method has the same functionally as
     * {@link AuthorDao#read(Serializable)}
     */
    @Override
    public Author loadAuthor(long authorId) throws ServiceException, NoSuchEntityException {
        Author author;

        try {
            author = authorDao.read(authorId);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }

        if (author == null) {
            throw new NoSuchEntityException(String.format("Author with id: %d doesn't exist", authorId));
        }

        return author;
    }

    /**
     * Implementation of
     * {@link AuthorService#loadAuthorByNewsId(long)} method
     * This method has the same functionally as
     * {@link AuthorDao#getAuthorByNewsId(Long)}
     */
    @Override
    public Author loadAuthorByNewsId(long newsId) throws ServiceException, NoSuchEntityException {
        Author author;

        try {
            author = authorDao.getAuthorByNewsId(newsId);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }

        if (author == null) {
            throw new NoSuchEntityException(String.format("Author for news id: %d doesn't exist", newsId));
        }

        return author;
    }

    /**
     * Implementation of
     * {@link AuthorService#loadAllAuthors()} method
     * This method has the same functionally as
     * {@link AuthorDao#getAllAuthors()}
     */
    @Override
    public List<Author> loadAllAuthors() throws ServiceException {
        List<Author> authors;

        try {
            authors = authorDao.getAllAuthors();
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }

        return authors;
    }

    /**
     * Implementation of
     * {@link AuthorService#loadActualAuthors()} method
     * This method has the same functionally as
     * {@link AuthorDao#getActualAuthors()}
     */
    @Override
    public List<Author> loadActualAuthors() throws ServiceException {
        List<Author> actualAuthors;

        try {
            actualAuthors = authorDao.getActualAuthors();
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }

        return actualAuthors;
    }
}
