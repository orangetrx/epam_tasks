package com.epam.newsmanager.service;

import com.epam.newsmanager.bean.Tag;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;

import java.util.List;
/**
 * TagService is a common interface for service layer, implementation of which
 * is using for operating TAGS table in database;
 */
public interface TagService {
    /**
     * This method is using for adding few Tag objects in database.
     *
     * @param tags
     *            list of TAG objects that contains all filled fields.
     * @throws ServiceException
     *             this exception throws when on service layer (e.g.)
     *             DaoException caught.
     */
    void addTags(List<Tag> tags) throws ServiceException;

    /**
     * This method is using for creating new TAG object in database.
     *
     * @param tag
     *            TAG object {@link Tag}that contains all filled fields.
     * @return If succeed - returns ID(Long value) in database of created TAG,
     *         otherwise null;
     * @throws ServiceException
     *             this exception throws when on service layer (e.g.)
     *             DaoException caught.
     */
    Long addTag(Tag tag) throws ServiceException;

    /**
     * This method is using for loading the TAG object(row) from database if it
     * exists.
     *
     * @param id
     *            ID of searching TAG.
     * @return tag object {@link Tag} with a specified TAG ID. If it doesn't exists, returns null.
     * @throws ServiceException
     *             this exception throws when on service layer (e.g.)
     *             DaoException caught.
     */
    Tag getTagById(long id) throws ServiceException, NoSuchEntityException;

    /**
     * This method is using for loading all tags from table TAG from database;
     *
     * @return list of existing tags in database;
     * @throws ServiceException
     *             this exception throws when on service layer (e.g.)
     *             DaoException caught.
     */
    List<Tag> getAllTags() throws ServiceException;

    /**
     * This method is using for loading tags for news from table TAG from database;
     * @param idNews ID of news
     * @return list of existing tags in database;
     * @throws ServiceException
     *             this exception throws when on service layer (e.g.)
     *             DaoException caught.
     */
    List<Tag> getTagsForNews(long idNews) throws ServiceException;
}
