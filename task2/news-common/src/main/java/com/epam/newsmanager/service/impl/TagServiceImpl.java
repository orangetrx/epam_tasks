package com.epam.newsmanager.service.impl;

import com.epam.newsmanager.bean.Tag;
import com.epam.newsmanager.dao.TagDao;
import com.epam.newsmanager.exception.DaoException;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
import com.epam.newsmanager.service.TagService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;
/**
 * TagServiceImpl is an implementation of
 * {@link TagService} interface. At this point
 * this class are injected by {@link TagDao} objects via Spring. So at now it
 * have the same functionality. But also it can be expanded by another
 * functional methods. NOTE:in all implemented methods added checking for
 * passing null values;
 *
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class TagServiceImpl implements TagService {


    @Resource(name = "${daotype}TagDao")
    private TagDao tagDao;

    /**
     * Implementation of
     * {@link TagService#addTags(List)} method
     */
    @Override
    public void addTags(List<Tag> tags) throws ServiceException {
        try {
            for (Tag tag : tags) {
                tagDao.create(tag);
            }
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    /**
     * Implementation of
     * {@link TagService#addTag(Tag)} method
     * This method has the same functionally as
     * {@link TagDao#create(Object)}
     */
    @Override
    public Long addTag(Tag tag) throws ServiceException {
        Long idTag;

        try {
            idTag = tagDao.create(tag);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }

        if (idTag == null) {
            throw new ServiceException("Tag was not added");
        }
        return idTag;
    }

    /**
     * Implementation of
     * {@link TagService#getTagById(long)} method
     * This method has the same functionally as
     * {@link TagDao#read(Serializable)}
     */
    @Override
    public Tag getTagById(long id) throws ServiceException, NoSuchEntityException {
        Tag tag;

        try {
            tag = tagDao.read(id);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }

        if (tag == null) {
            throw new NoSuchEntityException(String.format("Tag with id: %d", id));
        }

        return tag;
    }

    /**
     * Implementation of
     * {@link TagService#getAllTags()} method
     * This method has the same functionally as
     * {@link TagDao#getAllTags()}
     */
    @Override
    public List<Tag> getAllTags() throws ServiceException {
        List<Tag> tags;

        try {
            tags = tagDao.getAllTags();
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }

        return tags;
    }

    /**
     * Implementation of
     * {@link TagService#getTagsForNews(long)} method
     * This method has the same functionally as
     * {@link TagDao#getTagsForNews(Long)}
     */
    @Override
    public List<Tag> getTagsForNews(long idNews) throws ServiceException {
        List<Tag> tags;

        try {
            tags = tagDao.getTagsForNews(idNews);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }

        return tags;
    }

    @Override
    public void updateTag(Tag tag) throws ServiceException {
        try {
            tagDao.update(tag);
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteTag(Tag tag) throws ServiceException {
        try {
            tagDao.delete(tag.getTagId());
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }
}
