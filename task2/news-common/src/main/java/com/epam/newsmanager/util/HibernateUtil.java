package com.epam.newsmanager.util;

import org.hibernate.Session;

public final class HibernateUtil {
    private HibernateUtil(){}
    
    public static void closeSession(Session session) {
        if (session != null && session.isOpen()) {
            session.close();
        }
    }
}
