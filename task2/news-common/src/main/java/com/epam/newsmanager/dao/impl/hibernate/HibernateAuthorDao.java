package com.epam.newsmanager.dao.impl.hibernate;

import com.epam.newsmanager.bean.Author;
import com.epam.newsmanager.dao.AuthorDao;
import com.epam.newsmanager.exception.DaoException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class HibernateAuthorDao implements AuthorDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Author> getActualAuthors() throws DaoException {
        List<Author> authors = new ArrayList<>();

        Session session = sessionFactory.getCurrentSession();
        authors = session.getNamedQuery("getActualAuthors").setCacheable(true).list();

        return authors;
    }

    @Override
    public List<Author> getAllAuthors() throws DaoException {
        List<Author> authors = new ArrayList<>();
        Session session = sessionFactory.getCurrentSession();
        authors = session.getNamedQuery("getAllAuthors").list();
        return authors;
    }

    @Override
    public Author getAuthorByNewsId(Long id) throws DaoException {
        Author author;
        Session session = null;
        session = sessionFactory.getCurrentSession();
        Query query1 = session.getNamedQuery("selectAuthorsByNewsId").setParameter("id", id);
        author = (Author) query1.setCacheable(true).list().get(0);

        return author;
    }

    @Override
    public void setExpired(Author author) throws DaoException {
        Session session = null;
        session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(author);
        session.flush();
    }

    @Override
    public Long create(Author newInstance) throws DaoException {
        Session session = null;
        session = sessionFactory.getCurrentSession();
        session.save(newInstance);

        return newInstance.getAuthorId();
    }

    @Override
    public Author read(Long id) throws DaoException {
        Session session = null;
        Author author = null;
        session = sessionFactory.getCurrentSession();
        author = session.get(Author.class, id);

        return author;
    }

    @Override
    public void update(Author updatingInstance) throws DaoException {
        Session session = null;
        session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(updatingInstance);
        session.flush();
    }

    @Override
    public void delete(Long id) throws DaoException {
        Session session = null;
        session = sessionFactory.getCurrentSession();
        Author author = new Author();
        author.setAuthorId(id);
        session.delete(author);
        session.flush();
    }

}
