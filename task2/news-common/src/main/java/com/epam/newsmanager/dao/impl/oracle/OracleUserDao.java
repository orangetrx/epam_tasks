package com.epam.newsmanager.dao.impl.oracle;

import com.epam.newsmanager.bean.User;
import com.epam.newsmanager.dao.UserDao;
import com.epam.newsmanager.exception.DaoException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * OracleAuthorDao class is a specific realization of
 * {@link UserDao} interface. According to this
 * class you can interact with database through simple JDBC. Also it's using
 * injection of dataSource through Spring.
 */
@Component
@Scope("singleton")
public class OracleUserDao implements UserDao {

    private static final Logger LOGGER = Logger.getLogger(OracleUserDao.class.getPackage().getName());

    private static final String SQL_INSERT_USER_QUERY = "insert into \"USERS\" (USR_NAME, USR_LOGIN, USR_PASSWORD) values (?, ?, ?)";
    private static final String SQL_SELECT_USER_BY_ID_QUERY = "select USR_NAME, USR_LOGIN, USR_PASSWORD from \"USERS\" where USR_ID = ?";
    private static final String SQL_UPDATE_USER_QUERY = "update \"USERS\" SET USR_NAME = ?, USR_LOGIN=?, USR_PASSWORD=? WHERE USR_ID=?";
    private static final String SQL_DELETE_USER_QUERY = "delete FROM \"USERS\" WHERE USR_ID = ?";
    private static final String SQL_CHECK_USER_QUERY = "select USR_ID, USR_NAME, USR_LOGIN FROM USERS WHERE USR_LOGIN=? and USR_PASSWORD=?";
    private static final String SQL_SELECT_USER_BY_LOGIN_QUERY = "SELECT  USR_ID, USR_LOGIN, USR_NAME FROM USERS WHERE USR_LOGIN=?";

    /**
     * Some datasource that already configured for concrete
     *            database(e.g for oracle url/pass/login/driver).
     *            Annotation @Autowired is using for spring DI of DataSource.
     *            Configuration for oracle database you can see in
     *            database.properties file.
     */
    @Autowired
    private DataSource dataSource;

    /**
     * Implementation of
     * {@link UserDao#create(Object)} method through
     * simple JDBC.
     */
    public Long create(User newInstance) throws DaoException {

        Long id = null;
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_USER_QUERY, new String[]{"USR_ID"})) {
            statement.setString(1, newInstance.getUserName());
            statement.setString(2, newInstance.getLogin());
            statement.setString(3, newInstance.getPassword());
            statement.execute();
            try(ResultSet resultSet = statement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    id = resultSet.getLong(1);
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Create user dao error");
            throw new DaoException("Error insert into db", e);
        }

        return id;
    }

    /**
     * Implementation of
     * {@link UserDao#read(Serializable)} method through
     * simple JDBC.
     */
    public User read(Long id) throws DaoException {
        User user = null;

        try (Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_USER_BY_ID_QUERY)) {
            preparedStatement.setLong(1, id);
            try(ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    user = new User();
                    user.setUserName(resultSet.getString(1));
                    user.setLogin(resultSet.getString(2));
                    user.setPassword(resultSet.getString(3));
                    user.setUserId(id);
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Read user dao error");
            throw new DaoException("Error readByName from db", e);
        }

        return user;
    }

    /**
     * Implementation of
     * {@link UserDao#update(Object)} method through
     * simple JDBC.
     */
    public void update(User updatingInstance) throws DaoException {

        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_USER_QUERY)) {

            preparedStatement.setString(1, updatingInstance.getUserName());
            preparedStatement.setString(2, updatingInstance.getLogin());
            preparedStatement.setString(3, updatingInstance.getPassword());
            preparedStatement.setLong(4, updatingInstance.getUserId());
            preparedStatement.execute();
        } catch (SQLException e) {
            LOGGER.error("Update user dao error");
            throw new DaoException("Error update db", e);
        }
    }

    /**
     * Implementation of
     * {@link UserDao#delete(Serializable)} method through
     * simple JDBC.
     */
    @Override
    public void delete(Long id) throws DaoException {

        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_DELETE_USER_QUERY);
        ) {
            statement.setLong(1, id);
            statement.execute();
        } catch (SQLException e) {
            LOGGER.error("Delete user dao error");
            throw new DaoException("Error delete from db", e);
        }
    }

    @Override
    public User checkUser(User user) throws DaoException {
        User checkedUser = null;
        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_CHECK_USER_QUERY)) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    checkedUser = user;
                    checkedUser.setUserId(resultSet.getInt(1));
                    checkedUser.setUserName(resultSet.getString(2));
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Check user dao error");
            throw new DaoException("Error select from db", e);
        }

        return user;
    }

    @Override
    public User readUserByLogin(String login) throws DaoException {
        User user = null;
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_USER_BY_LOGIN_QUERY)) {
            statement.setString(1, login);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    user = new User();
                    user.setUserId(resultSet.getInt(1));
                    user.setUserName(resultSet.getString(3));
                    user.setLogin(resultSet.getString(2));
                }
            }
        } catch (SQLException e) {
            LOGGER.error("read user dao error");
            throw new DaoException("Error select from db", e);
        }

        return user;
    }
}
