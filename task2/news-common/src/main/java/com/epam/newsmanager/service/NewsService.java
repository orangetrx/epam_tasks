package com.epam.newsmanager.service;

import com.epam.newsmanager.bean.News;
import com.epam.newsmanager.bean.transferobject.SearchCriteria;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
import com.epam.newsmanager.util.PaginationData;

import java.util.List;

/**
 * NewsService is a common interface for service layer, implementation of which
 * is using for operating NEWS table in database;
 */
public interface NewsService {

    /**
     * This method is using for creating new NEWS object, creating links between news and authors
     * and between news and tags in database.
     *
     * @param news
     *             {@link News} object that contains all filled fields.
     * @return If succeed - returns ID(Long value) in database of created NEWS,
     *         otherwise null;
     * @throws ServiceException
     *             this exception throws when on service layer (e.g.)
     *             DaoException caught.
     */
    Long addNews(News news) throws ServiceException, NoSuchEntityException;


    /**
     * This method is using for creating new link between news and author in the database
     *
     * @param idNews
     *            ID of NEWS object to connect.
     * @param idAuthor
     *            ID of AUTHOR object to connect.
     * @throws ServiceException
     *             this exception throws when on service layer (e.g.)
     *             DaoException caught.
     */
    boolean insertNewsAuthorLink(Long idAuthor, Long idNews) throws ServiceException;

    /**
     * This method is using for creating new link between news and tag in the database
     * @param idNews
     *            ID of NEWS object to connect.
     * @param idTag
     *            ID of TAG object to connect.
     * @throws ServiceException
     *             this exception throws when on service layer (e.g.)
     *             DaoException caught.
     */
    boolean insertNewsTagLink(Long idNews, Long idTag) throws ServiceException;

    /**
     * This method is using for updating NEWS object in database.
     *
     * @param news
     *            NEWS object that contains all filled fields.
     * @throws ServiceException
     *             this exception throws when on service layer (e.g.)
     *             DaoException caught.
     */
    void editNews(News news) throws ServiceException;

    /**
     * This method is using for searching newsTO by SearchCriteria. Full
     * description of building query for searching you can find in QueryBuilder
     * class; NOTE: if buildQueryFromSearchCriteria method return empty
     * String(cause of both parameters in SearchCriteria object is null or
     * missing), result list of news will contain all news sorted by number of
     * comments and modification date;
     *
     * @param searchCriteria
     *            {@link SearchCriteria} object
     *            that must contain all filled fields(for more info look method
     *            description);
     * @return list of news that satisfactory by passed parameters(for other
     *         cases look method description)
     * @throws ServiceException
     *             this exception throws when on service layer (e.g.)
     *             DaoException caught.
     */
    List<News> searchNews(SearchCriteria searchCriteria, PaginationData paginationData) throws ServiceException;

    int getCountSearchedNews(SearchCriteria searchCriteria) throws ServiceException;

    /**
     * This method is using for loading the NEWS object(row) from database if it
     * exists.
     *
     * @param newsId
     *            ID of searching NEWS.
     * @return News object with a specified NEWS ID. If it doesn't exists,
     *         returns null.
     * @throws ServiceException
     *             this exception throws when on service layer (e.g.)
     *             DaoException caught.
     */
    News viewNews(Long newsId) throws ServiceException, NoSuchEntityException;


    /**
     * This method is using fore loading all news that are in database.
     *
     * @return list of news sorted by number of comments;
     * @throws ServiceException
     *             this exception throws when on service layer (e.g.)
     *             DaoException caught.
     */
    List<News> viewAllNews() throws ServiceException;

    List<News> viewNewsPerPage(PaginationData paginationData) throws ServiceException;

    /**
     * This method is using for deleting NEWS object(row) from database by ID of
     * NEWS and deleting all links for tags and author.
     *
     * @param newsId
     *            ID of NEWS that are going to be deleted.
     * @throws ServiceException
     *             this exception throws when on service layer (e.g.)
     *             DaoException caught.
     */
    void deleteNews(Long newsId) throws ServiceException;


    /**
     * This method is using for loading NEWS objects(row) from database
     * @return list of news sorted by number of comments;
     * @throws ServiceException
     *              this exception throws when on service layer (e.g.)
     *              DaoException caught.
     */
    List<News> viewSortedNews() throws ServiceException;

    /**
     * This method is using for loading count of News that stores in database
     * @return number of count news
     * @throws ServiceException
     */
    int getCountOfNews() throws ServiceException;

    List<Long> getAllNewsIds() throws ServiceException;
}
