package com.epam.newsmanager.dao.impl.eclipselink;

import com.epam.newsmanager.bean.Comment;
import com.epam.newsmanager.dao.CommentDao;
import com.epam.newsmanager.exception.DaoException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class EclipseLinkCommentDao implements CommentDao {

    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public List<Comment> getCommentsByNews(Long newsId) throws DaoException {
        return entityManager.createNamedQuery("getCommentsForNews").setParameter("newsId", newsId).getResultList();
    }

    @Override
    public boolean deleteCommentsByNewsId(Long newsId) throws DaoException {
        Comment comment = new Comment();
        comment.setNewsId(newsId);
        entityManager.remove(comment);
        return true;
    }

    @Override
    public int getCountOfCommentsByNewsId(Long newsId) throws DaoException {
        Query query = entityManager.createNamedQuery("getCountCommentsByNews").setParameter("newsId", newsId);
        return (int) query.getSingleResult();
    }

    @Override
    public Long create(Comment newInstance) throws DaoException {
        entityManager.persist(newInstance);
        entityManager.flush();
        return newInstance.getCommentId();
    }

    @Override
    public Comment read(Long id) throws DaoException {
        return entityManager.find(Comment.class, id);
    }

    @Override
    public void update(Comment updatingInstance) throws DaoException {
        entityManager.merge(updatingInstance);
    }

    @Override
    public void delete(Long id) throws DaoException {
        Comment comment = new Comment();
        comment.setCommentId(id);
        entityManager.remove(comment);
    }
}
