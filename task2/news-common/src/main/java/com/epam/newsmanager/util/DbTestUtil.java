package com.epam.newsmanager.util;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * This support class that have few static methods that performs
 * reset tables sequences to the default state for dropping autoincrement
 */
public class DbTestUtil {

    private static final String DROP_SEQ_TAGS = "DROP SEQUENCE \"TG_SEQ\"";
    private static final String CREATE_SEQ_TAGS ="CREATE SEQUENCE \"TG_SEQ\" MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE";

    public static void resetAutoIncrementInTags(DataSource dataSource) {
        resetSeq(dataSource, DROP_SEQ_TAGS, CREATE_SEQ_TAGS);
    }
    ////////////////////////////////////////


    private static final String DROP_SEQ_USERS = "DROP SEQUENCE \"USR_SEQ\"";
    private static final String CREATE_SEQ_USERS = "CREATE SEQUENCE \"USR_SEQ\" MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE";

    public static void resetAutoIncrementInUsers(DataSource dataSource) {
        resetSeq(dataSource, DROP_SEQ_USERS, CREATE_SEQ_USERS);
    }
    ////////////////////////////////////////


    private static final String DROP_SEQ_AUTHORS = "DROP SEQUENCE \"AUT_SEQ\"";
    private static final String CREATE_SEQ_AUTHORS = "CREATE SEQUENCE \"AUT_SEQ\" MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE";

    public static void resetAutoIncrementInAuthors(DataSource dataSource) {
        resetSeq(dataSource, DROP_SEQ_AUTHORS, CREATE_SEQ_AUTHORS);
    }
    ////////////////////////////////////////


    private static final String DROP_SEQ_NEWS = "DROP SEQUENCE \"NWS_SEQ\"";
    private static final String CREATE_SEQ_NEWS = "CREATE SEQUENCE \"NWS_SEQ\" MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE";

    public static void resetAutoIncrementInNews(DataSource dataSource) {
        resetSeq(dataSource, DROP_SEQ_NEWS, CREATE_SEQ_NEWS);
    }
    ////////////////////////////////////////

    private static final String DROP_SEQ_COMMENTS = "DROP SEQUENCE \"COM_SEQ\"";
    private static final String CREATE_SEQ_COMMENTS = "CREATE SEQUENCE \"COM_SEQ\" MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE";

    public static void resetAutoIncrementInComments(DataSource dataSource) {
        resetSeq(dataSource, DROP_SEQ_COMMENTS, CREATE_SEQ_COMMENTS);
    }
    ////////////////////////////////////////

    private static final String DROP_SEQ_ROLES = "DROP SEQUENCE \"ROL_SEQ\"";
    private static final String CREATE_SEQ_ROLES = "CREATE SEQUENCE \"ROL_SEQ\" MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE";

    public static void resetAutoIncrementInRoles(DataSource dataSource) {
        resetSeq(dataSource, DROP_SEQ_ROLES, CREATE_SEQ_ROLES);
    }

    private static void resetSeq(DataSource dataSource, String dropSeqQuery, String createSeqQuery) {
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            statement.execute(dropSeqQuery);
            statement.execute(createSeqQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
