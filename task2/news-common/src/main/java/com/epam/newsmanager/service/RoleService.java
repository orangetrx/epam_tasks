package com.epam.newsmanager.service;

import com.epam.newsmanager.bean.Role;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
/**
 * RoleService is a common interface for service layer, implementation of which
 * is using for operating ROLES table in database;
 */
public interface RoleService {
    /**
     * This method is using for creating new ROLE object in database
     * @param role Role object {@link Role} that contains all filled fields
     * @return If succeed - returns ID(Long value) in database of created ROLE,
     *         otherwise null;
     * @throws ServiceException
     */
    Long addRole(Role role) throws ServiceException, NoSuchEntityException;

    /**
     * This method is using for loading the ROLE object(row) from database if it
     * exists.
     * @param roleId ID of searching Role
     * @return Role object {@link Role} with a specified NEWS ID. If it doesn't exists,
     *         returns null.
     * @throws ServiceException
     */
    Role loadRole(long roleId) throws ServiceException, NoSuchEntityException;
}
