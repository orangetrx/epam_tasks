package com.epam.newsmanager.dao.impl.eclipselink;

import com.epam.newsmanager.bean.News;
import com.epam.newsmanager.bean.transferobject.SearchCriteria;
import com.epam.newsmanager.dao.NewsDao;
import com.epam.newsmanager.exception.DaoException;
import com.epam.newsmanager.util.PaginationData;
import com.epam.newsmanager.util.QueryBuilder;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class EclipseLinkNewsDao implements NewsDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<News> getAllNews() throws DaoException {
        return entityManager.createNamedQuery("getAllNews").getResultList();
    }

    @Override
    public List<News> getNewsPerPage(PaginationData paginationData) throws DaoException {
        return entityManager.createNamedQuery("getSortedNews")
                .setParameter(1, paginationData.getEndset())
                .setParameter(2, paginationData.getOffset()).getResultList();
    }

    @Override
    public int getCountOfAllNews() throws DaoException {
        return Integer.valueOf(entityManager.createNamedQuery("getCountNews").getSingleResult().toString());
    }

    @Override
    public boolean insertNewsAuthorLink(Long idAuthor, Long idNews) throws DaoException {
        entityManager.createNamedQuery("insertNewsAuthorLink").setParameter(1, idNews)
                .setParameter(2, idAuthor).executeUpdate();
        return true;
    }

    @Override
    public boolean insertNewsTagLink(Long idNews, Long idTag) throws DaoException {
        entityManager.createNamedQuery("insertNewsTagLink")
                .setParameter(1, idNews)
                .setParameter(2, idTag).executeUpdate();
        return true;
    }

    @Override
    public boolean deleteNewsAuthorLink(Long idNews) throws DaoException {
        entityManager.createNamedQuery("deleteNewsAuthorLink").setParameter(1, idNews).executeUpdate();
        return true;
    }

    @Override
    public boolean deleteNewsTagLink(Long idNews) throws DaoException {
        entityManager.createNamedQuery("deleteNewsTagLink").setParameter(1, idNews).executeUpdate();
        return true;
    }

    @Override
    public List<News> getNewsByTag(Long tagId) throws DaoException {
        throw new UnsupportedOperationException();
    }

    private static final String SQL_LEFT_PART_OF_QUERY_FOR_SEARCH_CRITERIA =
            "SELECT *\n" +
                    "  FROM ( SELECT a.*, ROWNUM rnum\n" +
                    "    FROM  (";
    private static final String SQL_RIGHT_PART_OF_QUERY_FOR_SEARCH_CRITERIA =
            ") a\n "+
                    "    WHERE ROWNUM <= ?1 )\n" +
                    "WHERE rnum > ?2";

    @Override
    public List<News> getNewsBySearchCriteria(SearchCriteria searchCriteria, PaginationData paginationData) throws DaoException {
        return entityManager.createNativeQuery(SQL_LEFT_PART_OF_QUERY_FOR_SEARCH_CRITERIA +
                QueryBuilder.getQueryByCriteria(searchCriteria) + SQL_RIGHT_PART_OF_QUERY_FOR_SEARCH_CRITERIA, News.class)
                .setParameter(1, paginationData.getEndset())
                .setParameter(2, paginationData.getOffset()).getResultList();
    }

    @Override
    public int getCountNewsBySearchCriteria(SearchCriteria searchCriteria) throws DaoException {
        return entityManager.createNativeQuery(QueryBuilder.getQueryByCriteria(searchCriteria)).getResultList().size();
    }

    private static final String JPQL_SELECT_NEWS_IDS = "select n.newsId FROM News n";

    @Override
    public List<Long> getAllNewsIdList() throws DaoException {
        return entityManager.createQuery(JPQL_SELECT_NEWS_IDS, Long.class).getResultList();
    }

    @Override
    public Long create(News newInstance) throws DaoException {
        entityManager.persist(newInstance);
        entityManager.flush();
        return newInstance.getNewsId();
    }

    @Override
    public News read(Long id) throws DaoException {
        return entityManager.find(News.class, id);
    }

    @Override
    public void update(News updatingInstance) throws DaoException {
        entityManager.merge(updatingInstance);
    }

    @Override
    public void delete(Long id) throws DaoException {
        News news = new News();
        news.setNewsId(id);
        entityManager.remove(news);
    }
}
