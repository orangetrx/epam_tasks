package com.epam.newsmanager.dao.impl.hibernate;

import com.epam.newsmanager.bean.Tag;
import com.epam.newsmanager.dao.TagDao;
import com.epam.newsmanager.exception.DaoException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class HibernateTagDao implements TagDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public List<Tag> getAllTags() throws DaoException {
        Session session;
        List<Tag> tags;
        session = sessionFactory.getCurrentSession();
        tags = session.getNamedQuery("selectAllTags").setCacheable(true).list();

        return tags;
    }

    @Override
    public List<Tag> getTagsForNews(Long idNews) throws DaoException {
        Session session;
        List<Tag> tags;
        session = sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery("selectTagsForNews").setParameter("id", idNews);
        tags = query.setCacheable(true).list();

        return tags;
    }

    @Override
    public Tag readByName(String tagName) throws DaoException {
        Session session = null;
        Tag tag = null;
        session = sessionFactory.getCurrentSession();
        tag = session.get(Tag.class, tagName);

        return tag;
    }

    @Override
    public Long create(Tag newInstance) throws DaoException {
        Session session = null;
        session = sessionFactory.getCurrentSession();
        session.save(newInstance);
        return newInstance.getTagId();
    }

    @Override
    public Tag read(Long id) throws DaoException {
        Session session = null;
        Tag tag = null;

        session = sessionFactory.getCurrentSession();
        tag = session.get(Tag.class, id);

        return tag;
    }

    @Override
    public void update(Tag updatingInstance) throws DaoException {
        Session session = null;
        session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(updatingInstance);
        session.flush();
    }

    @Override
    public void delete(Long id) throws DaoException {
        Session session = null;
        session = sessionFactory.getCurrentSession();
        Tag tag = new Tag();
        tag.setTagId(id);
        session.delete(tag);
        session.flush();
    }
}
