package com.epam.newsmanager.dao.impl.oracle;

import com.epam.newsmanager.bean.Comment;
import com.epam.newsmanager.dao.CommentDao;
import com.epam.newsmanager.exception.DaoException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.io.Serializable;
import java.sql.*;
import java.util.*;
import java.util.Date;

/**
 * OracleCommentDao class is a specific realization of
 * {@link CommentDao} interface. According to this
 * class you can interact with database through simple JDBC.
 */
@Component
@Scope("singleton")
public class OracleCommentDao implements CommentDao {

    private static final Logger LOGGER = Logger.getLogger(OracleCommentDao.class.getPackage().getName());

    private static final String SQL_SELECT_COMMENTS_BY_NEWS_QUERY = "select COM_ID, COM_NEWS_ID, COM_TEXT, COM_CREATION_DATE from \"COMMENTS\" where COM_NEWS_ID = ? " +
            "ORDER BY COM_CREATION_DATE DESC ";
    private static final String SQL_SELECT_COUNT_OF_COMMENTS_BY_NEWS = "select count(COM_ID) FROM \"COMMENTS\" WHERE COM_NEWS_ID = ?";
    private static final String SQL_INSERT_COMMENT_QUERY = "insert into \"COMMENTS\"  (COM_NEWS_ID, COM_TEXT, COM_CREATION_DATE) " +
                                                        "values (?, ?, ?)";
    private static final String SQL_SELECT_COMMENT_BY_ID_QUERY = "select COM_ID, COM_NEWS_ID, COM_TEXT, COM_CREATION_DATE from \"COMMENTS\" where COM_ID = ?";
    private static final String SQL_DELETE_COMMENTS_BY_NEWS_ID = "delete from \"COMMENTS\" where COM_NEWS_ID = ?";
    private static final String SQL_DELETE_COMMENT_BY_ID = "delete from \"COMMENTS\" where COM_ID = ?";

    /**
     * Some datasource that already configured for concrete
     *            database(e.g for oracle url/pass/login/driver).
     *            Annotation @Autowired is using for spring DI of DataSource.
     *            Configuration for oracle database you can see in
     *            database.properties file.
     */
    @Autowired
    private DataSource dataSource;

    /**
     * Implementation of
     * {@link CommentDao#getCommentsByNews(Long)} method
     * through simple JDBC.
     */
    @Override
    public List<Comment> getCommentsByNews(Long id) throws DaoException {
        List<Comment> comments = new ArrayList<>();
        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_COMMENTS_BY_NEWS_QUERY)) {
            statement.setLong(1, id);
            try(ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Comment comment = new Comment();
                    comment.setCommentId(resultSet.getLong(1));
                    comment.setNewsId(resultSet.getLong(2));
                    comment.setCommentText(resultSet.getString(3));
                    comment.setCreationDate(new java.util.Date(resultSet.getTimestamp(4).getTime()));
                    comments.add(comment);
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Select comments for news error");
            throw new DaoException("Error select from db", e);
        }

        return comments;
    }

    /**
     * Implementation of
     * {@link CommentDao#deleteCommentsByNewsId(Long)}method
     * through simple JDBC.
     */
    @Override
    public boolean deleteCommentsByNewsId(Long newsId) throws DaoException {
        boolean result = false;

        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_DELETE_COMMENTS_BY_NEWS_ID)) {
            statement.setLong(1, newsId);
            result = statement.execute();
        } catch (SQLException e) {
            LOGGER.error("Delete comments news error");
            throw new DaoException("Error delete from db", e);
        }

        return result;
    }

    /**
     * Implementation of
     * {@link CommentDao#getCountOfCommentsByNewsId(Long)} method
     * through simple JDBC.
     */
    @Override
    public int getCountOfCommentsByNewsId(Long newsId) throws DaoException {
        int countComments = 0;
        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_COUNT_OF_COMMENTS_BY_NEWS)) {
            statement.setLong(1, newsId);
            try(ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    countComments = resultSet.getInt(1);
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Get count of comments for news error");
            throw new DaoException("Error select from db", e);
        }

        return countComments;
    }

    /**
     * Implementation of
     * {@link CommentDao#create(Object)}method
     * through simple JDBC.
     */
    @Override
    public Long create(Comment newInstance) throws DaoException {
        Long id = null;

        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT_COMMENT_QUERY, new String[]{"COM_ID"})) {
            statement.setLong(1, newInstance.getNewsId());
            statement.setString(2, newInstance.getCommentText());
            statement.setTimestamp(3, new Timestamp(newInstance.getCreationDate().getTime()));

            statement.execute();

            try(ResultSet resultSet = statement.getGeneratedKeys()) {
                if (resultSet.next())
                id = resultSet.getLong(1);
            }

        } catch (SQLException e) {
            LOGGER.error("insert comment error");
            throw new DaoException("Error insert into db", e);
        }

        return id;
    }

    /**
     * Implementation of
     * {@link CommentDao#read(Serializable)}method
     * through simple JDBC.
     */
    @Override
    public Comment read(Long id) throws DaoException {
        Comment comment = null;

        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_COMMENT_BY_ID_QUERY)) {
            statement.setLong(1, id);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    comment = new Comment();
                    comment.setCommentId(resultSet.getLong(1));
                    comment.setNewsId(resultSet.getLong(2));
                    comment.setCommentText(resultSet.getString(3));
                    comment.setCreationDate(new Date(resultSet.getTimestamp(4).getTime()));
                }
            }
        } catch (SQLException e) {
            LOGGER.error("select comment error");
            throw new DaoException("Error select from db", e);
        }

        return comment;
    }

    /**
     * Implementation of
     * {@link CommentDao#update(Object)}method
     * through simple JDBC.
     * In this case operation isn't available
     */
    @Override
    public void update(Comment updatingInstance) throws DaoException {
        throw new UnsupportedOperationException("Comment updating is unsupported");
    }
    /**
     * Implementation of
     * {@link CommentDao#delete(Serializable)}method
     * through simple JDBC.
     *
     */
    @Override
    public void delete(Long id) throws DaoException {

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_DELETE_COMMENT_BY_ID)) {
            statement.setLong(1, id);
            statement.execute();
        } catch (SQLException e) {
            LOGGER.error("delete comment error");
            throw new DaoException("Error delete from db", e);
        }
    }
}
