package com.epam.newsmanager.dao.impl.hibernate;

import com.epam.newsmanager.bean.News;
import com.epam.newsmanager.bean.transferobject.SearchCriteria;
import com.epam.newsmanager.dao.NewsDao;
import com.epam.newsmanager.exception.DaoException;
import com.epam.newsmanager.util.PaginationData;
import com.epam.newsmanager.util.QueryBuilder;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class HibernateNewsDao implements NewsDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<News> getAllNews() throws DaoException {
        List<News> newses = new ArrayList<>();

        Session session = sessionFactory.getCurrentSession();
        newses = session.getNamedQuery("getAllNews").setCacheable(true).list();
        return newses;
    }

    @Override
    public List<News> getNewsPerPage(PaginationData paginationData) throws DaoException {
        List<News> newses = new ArrayList<>();

        Session session = sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery("getSortedNews")
                .setParameter("endset", paginationData.getEndset())
                .setParameter("offset", paginationData.getOffset());

        newses = query.setCacheable(true).list();

        return newses;
    }

    @Override
    public int getCountOfAllNews() throws DaoException {
        Session session = null;
        int count = 0;
        session = sessionFactory.getCurrentSession();
        count = (int) (long) session.createCriteria(News.class).setProjection(Projections.rowCount()).uniqueResult();

        return count;
    }

    @Override
    public boolean insertNewsAuthorLink(Long idAuthor, Long idNews) throws DaoException {
        Session session = null;
        boolean result = false;
        session = sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery("insertNewsAuthorLink")
                .setParameter("newsId", idNews)
                .setParameter("authorId", idAuthor);
        query.executeUpdate();
        result = true;

        return result;
    }

    @Override
    public boolean insertNewsTagLink(Long idNews, Long idTag) throws DaoException {
        Session session = null;
        boolean result = false;
        session = sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery("insertNewsTagLink")
                .setParameter("newsId", idNews)
                .setParameter("tagId", idTag);
        query.executeUpdate();
        result = true;

        return result;
    }

    @Override
    public boolean deleteNewsAuthorLink(Long idNews) throws DaoException {
        Session session = null;
        boolean result = false;
        session = sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery("deleteNewsAuthorLink")
                .setParameter("newsId", idNews);
        result = true;
        query.executeUpdate();
        session.flush();

        return result;
    }

    @Override
    public boolean deleteNewsTagLink(Long idNews) throws DaoException {
        Session session = null;
        boolean result = false;
        session = sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery("deleteNewsTagLink")
                .setParameter("newsId", idNews);
        result = true;
        query.executeUpdate();
        session.flush();

        return result;
    }

    @Override
    public List<News> getNewsByTag(Long tagId) throws DaoException {
        throw new UnsupportedOperationException();
    }

    private static final String HQL_LEFT_PART_OF_QUERY_FOR_SEARCH_CRITERIA =
            "SELECT *\n" +
                    "  FROM ( SELECT a.*, ROWNUM rnum\n" +
                    "    FROM  (";
    private static final String HQL_RIGHT_PART_OF_QUERY_FOR_SEARCH_CRITERIA =
            ") a\n " +
                    "    WHERE ROWNUM <= :endset )\n" +
                    "WHERE rnum > :offset";

    @Override
    public List<News> getNewsBySearchCriteria(SearchCriteria searchCriteria, PaginationData paginationData) throws DaoException {
        List<News> newses = new ArrayList<>();
        Session session = null;
        session = sessionFactory.getCurrentSession();
        Query query = session.createSQLQuery(HQL_LEFT_PART_OF_QUERY_FOR_SEARCH_CRITERIA +
                QueryBuilder.getQueryByCriteria(searchCriteria) + HQL_RIGHT_PART_OF_QUERY_FOR_SEARCH_CRITERIA)
                .addEntity(News.class)
                .setParameter("endset", paginationData.getEndset())
                .setParameter("offset", paginationData.getOffset());

        newses = query.setCacheable(true).list();

        return newses;
    }

    @Override
    public int getCountNewsBySearchCriteria(SearchCriteria searchCriteria) throws DaoException {
        int count = 0;
        Session session = null;
        session = sessionFactory.getCurrentSession();
        Query query = session.createSQLQuery(QueryBuilder.getQueryByCriteria(searchCriteria))
                .addEntity(News.class);
        count = query.setCacheable(true).list().size();

        return count;
    }

    private static final String SQL_SELECT_NEWS_IDS = "select NEWS.NWS_ID FROM NEWS";


    @Override
    public List<Long> getAllNewsIdList() throws DaoException {
        List<Long> newsIds = new ArrayList<>();
        Session session = null;
        session = sessionFactory.getCurrentSession();
        Query query = session.createSQLQuery(SQL_SELECT_NEWS_IDS).addScalar("NWS_ID", StandardBasicTypes.LONG);
        newsIds = query.setCacheable(true).list();

        return newsIds;
    }

    @Override
    public Long create(News newInstance) throws DaoException {
        Session session = null;
        session = sessionFactory.getCurrentSession();
        session.save(newInstance);

        return newInstance.getNewsId();
    }

    @Override
    public News read(Long id) throws DaoException {
        News news = null;
        Session session = null;
        session = sessionFactory.getCurrentSession();
        news = session.get(News.class, id);

        return news;
    }

    @Override
    public void update(News updatingInstance) throws DaoException {
        Session session = null;
        session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(updatingInstance);
        session.flush();
    }

    @Override
    public void delete(Long id) throws DaoException {
        Session session = null;
        session = sessionFactory.getCurrentSession();
        News news = new News();
        news.setNewsId(id);
        session.delete(news);
        session.flush();
    }
}
