package com.epam.newsmanager.service;

import com.epam.newsmanager.bean.Comment;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;

import java.util.List;

/**
 * CommentService is a common interface for service layer, implementation of
 * which is using for operating COMMENTS table in database;
 */
public interface CommentService {

    /**
     * This method is using for creating new COMMENT object in database.
     *
     * @param comment
     *            COMMENT object that contains all filled fields.
     * @return If succeed - returns ID(Long value) in database of created
     *         COMMENT, otherwise null;
     * @throws ServiceException
     *             this exception throws when on service layer (e.g.)
     *             DaoException caught.
     */
    Long addComment(Comment comment) throws ServiceException, NoSuchEntityException;

    /**
     * This method is using for deleting COMMENT object(row) from database by ID
     * of COMMENT.
     *
     * @param commentId
     *            ID of COMMENT that are going to be deleted.
     * @throws ServiceException
     *             this exception throws when on service layer (e.g.)
     *             DaoException caught.
     */
    void deleteComment(Long commentId) throws ServiceException;

    /**
     * This method is using for deleting COMMENT object by NEWS ID.
     *
     * @param newsId
     *            ID of NEWS with whom this comment is connected.
     * @throws ServiceException
     *             this exception throws when on service layer (e.g.)
     *             DaoException caught.
     */
    void deleteCommentByNewsId(Long newsId) throws ServiceException;


    /**
     * This method is using for loading the COMMENT object(row) from database if
     * it exists
     *
     * @param commentId
     *            ID of searching COMMENT.
     * @return COMMENT object with a specified COMMENT ID. If it doesn't exists,
     *         returns not null COMMENT object, with null fields.
     * @throws ServiceException
     *             this exception throws when on service layer (e.g.)
     *             DaoException caught.
     */
    Comment loadComment(Long commentId) throws ServiceException, NoSuchEntityException;

    /**
     * This method is using for loading comments for news from database
     * @param newsId ID of news
     * @return list of Comment objects {@link Comment}
     * @throws ServiceException
     */
    List<Comment> loadCommentByNews(Long newsId) throws ServiceException;

}
