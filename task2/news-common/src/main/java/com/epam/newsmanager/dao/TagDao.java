package com.epam.newsmanager.dao;

import com.epam.newsmanager.bean.Tag;
import com.epam.newsmanager.exception.DaoException;

import java.util.List;

/**
 * TagDao is a common interface for interactions with table TAG in database. In
 * order to interact with this table, implement this interface and write you
 * realization for particular database;
 * This interface extends by generic interface that contains C.R.U.D. operations {@link GenericDao}
 */
public interface TagDao extends GenericDao<Tag, Long> {
    /**
     * This method is using for loading list of all Tag objects from TAGS table
     * @return List of Tag objects {@link Tag}
     * @throws DaoException
     *              this exception throws when on dao layer (e.g.) SQLException
     *              caught.
     */
    List<Tag> getAllTags() throws DaoException;

    /**
     * This method is using for loading list of Tag objects for selected news from TAGS table
     * @param idNews ID of news
     * @return list of Tag objects {@link Tag}
     * @throws DaoException
     *              this exception throws when on dao layer (e.g.) SQLException
     *              caught.
     */
    List<Tag> getTagsForNews(Long idNews) throws DaoException;

    /**
     * This method is using for loading Tag object by tag's name
     * @param tagName name of Tag
     * @return Tag object {@link Tag} of null if table doesn't contain tag with current name
     * @throws DaoException
     *              this exception throws when on dao layer (e.g.) SQLException
     *              caught.
     */
    Tag readByName(String tagName) throws DaoException;
}
