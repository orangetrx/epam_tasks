package com.epam.newsmanager.bean;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Tag class is representing tag model for table TAG in database. TAG table
 * fields are TG_ID;TG_NAME;
 */
@Entity
@Table(name = "TAGS")
@NamedQueries({
        @NamedQuery(name = "getAllTags", query = "From Tag tag"),
        @NamedQuery(name = "getTagByName", query = "From Tag t where t.tagName = :name")
})

@NamedNativeQueries({
        @NamedNativeQuery(name = "getTagsForNews", query = "SELECT * FROM TAGS INNER JOIN NEWS_TAGS ON NEWS_TAGS.NWT_NEWS_ID  = ?1 and TG_ID = NEWS_TAGS.NWT_TAG_ID",
        resultClass = Tag.class)
})
public class Tag implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "TG_ID")
    private long tagId;

    @Column(name = "TG_NAME")
    @NotNull
    @Size(min = 1,max = 30)
    private String tagName;

    @Column(name = "TG_VERSION")
    @Version
    private Integer version;

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public long getTagId() {
        return tagId;
    }

    public void setTagId(long tagId) {
        this.tagId = tagId;
    }

}
