package com.epam.newsmanager.service.impl;

import com.epam.newsmanager.bean.News;
import com.epam.newsmanager.bean.transferobject.SearchCriteria;
import com.epam.newsmanager.dao.AuthorDao;
import com.epam.newsmanager.dao.CommentDao;
import com.epam.newsmanager.dao.NewsDao;
import com.epam.newsmanager.dao.TagDao;
import com.epam.newsmanager.exception.DaoException;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
import com.epam.newsmanager.service.NewsService;
import com.epam.newsmanager.util.PaginationData;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * NewsServiceImpl is an implementation of
 * {@link NewsService} interface. At this point
 * this class are injected by {@link NewsDao}, {@link AuthorDao}, {@link CommentDao},
 * {@link TagDao} objects via Spring. So at now it have the same functionality. But also it can be
 * expanded by another functional methods.
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class NewsServiceImpl implements NewsService {

    @Resource(name = "${daotype}NewsDao")
    private NewsDao newsDao;

    /**
     * Implementation of
     * {@link NewsService#addNews(News)} method
     */
    @Override
    public Long addNews(News news) throws ServiceException, NoSuchEntityException {
        Long idNews;

        try {
            news.setCreationDate(new Date());
            news.setModificationDate(new Date());
            idNews = newsDao.create(news);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }

        if (idNews == null) {
            throw new NoSuchEntityException("News was not added");
        }

        return idNews;
    }

    /**
     * Implementation of
     * {@link NewsService#insertNewsAuthorLink(Long, Long)} method
     */
    @Override
    public boolean insertNewsAuthorLink(Long idAuthor, Long idNews) throws ServiceException {
        boolean result;
        try {
            result = newsDao.insertNewsAuthorLink(idAuthor, idNews);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return result;
    }

    /**
     * Implementation of
     * {@link NewsService#insertNewsTagLink(Long, Long)}  method
     */
    @Override
    public boolean insertNewsTagLink(Long idNews, Long idTag) throws ServiceException {
        boolean result;
        try {
            result = newsDao.insertNewsTagLink(idNews, idTag);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return result;
    }

    /**
     * Implementation of
     * {@link NewsService#editNews(News)} method
     */
    @Override
    public void editNews(News news) throws ServiceException {
        try {
            news.setModificationDate(new Date());
            news.setCreationDate(new Date());
            newsDao.update(news);

            newsDao.deleteNewsAuthorLink(news.getNewsId());
            newsDao.deleteNewsTagLink(news.getNewsId());
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    /**
     * Implementation of
     * {@link NewsService#searchNews(SearchCriteria, PaginationData)} method
     */
    @Override
    public List<News> searchNews(SearchCriteria searchCriteria, PaginationData paginationData) throws ServiceException {
        List<News> newsList;

        try {
            newsList = newsDao.getNewsBySearchCriteria(searchCriteria, paginationData);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }

        return newsList;
    }

    @Override
    public int getCountSearchedNews(SearchCriteria searchCriteria) throws ServiceException {
        int countNews = 0;
        try {
            countNews = newsDao.getCountNewsBySearchCriteria(searchCriteria);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }

        return countNews;
    }

    /**
     * Implementation of
     * {@link NewsService#viewNews(Long)} method
     */
    @Override
    public News viewNews(Long newsId) throws ServiceException, NoSuchEntityException {
        News news = null;

        try {
            news = newsDao.read(newsId);

        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }

        if (news == null) {
            throw new NoSuchEntityException(String.format("news with id: %d doesn't exist", newsId));
        }

        return news;
    }

    /**
     * Implementation of
     * {@link NewsService#viewAllNews()} method
     */
    @Override
    public List<News> viewAllNews() throws ServiceException {
        List<News> newsList;

        try {
            newsList = newsDao.getAllNews();
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return newsList;
    }

    @Override
    public List<News> viewNewsPerPage(PaginationData paginationData) throws ServiceException {
        List<News> newsList;

        try {
            newsList = newsDao.getNewsPerPage(paginationData);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return newsList;
    }

    /**
     * Implementation of
     * {@link NewsService#deleteNews(Long)} method
     */
    @Override
    public void deleteNews(Long newsId) throws ServiceException {

        try {
            newsDao.deleteNewsAuthorLink(newsId);
            newsDao.deleteNewsTagLink(newsId);
            newsDao.delete(newsId);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    /**
     * Implementation of
     * {@link NewsService#viewSortedNews()} method
     */
    @Override
    public List<News> viewSortedNews() throws ServiceException {
        List<News> sortedNews = null;
        try {
            sortedNews = newsDao.getAllNews();
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }

        return sortedNews;
    }

    /**
     * Implementation of
     * {@link NewsService#getCountOfNews()} method
     */
    @Override
    public int getCountOfNews() throws ServiceException {
        int countNews;

        try {
            countNews = newsDao.getCountOfAllNews();
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }

        return countNews;
    }

    @Override
    public List<Long> getAllNewsIds() throws ServiceException {
        List<Long> idList;

        try {
            idList = newsDao.getAllNewsIdList();
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }

        return idList;
    }
}
