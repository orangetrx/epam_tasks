package com.epam.newsmanager.dao;

import com.epam.newsmanager.bean.Comment;
import com.epam.newsmanager.exception.DaoException;

import java.util.List;

/**
 * CommentDao is a common interface for interactions with table COMMENTS in
 * database. In order to interact with this table, implement this interface and
 * write you realization for particular database;
 * This interface extends by generic interface that contains C.R.U.D. operations {@link GenericDao}
 */
public interface CommentDao extends GenericDao<Comment, Long> {
    /**
     * This method is using for loading list of Comment object from table COMMENTS that appears comments of due news
     * @param newsId identifier of news
     * @return returns list of found Comments or empty list if comments for news doesn't exist
     * @throws DaoException
     *              this exception throws when on dao layer (e.g.) SQLException
     *              caught.
     */
    List<Comment> getCommentsByNews(Long newsId) throws DaoException;

    /**
     * This method is using for deleting COMMENT object by NEWS ID.
     *
     * @param newsId ID of NEWS with whom comments is connected.
     * @throws DaoException
     *             this exception throws when on dao layer (e.g.) SQLException
     *             caught.
     */
    boolean deleteCommentsByNewsId(Long newsId) throws DaoException;

    /**
     * This method is using for loading count for due news
     * @param newsId ID of NEWS with whom comments is connected.
     * @return number of comments that appears comments of due news
     * @throws DaoException
     *              this exception throws when on dao layer (e.g.) SQLException
     *              caught.
     */
    int getCountOfCommentsByNewsId(Long newsId) throws DaoException;
}
