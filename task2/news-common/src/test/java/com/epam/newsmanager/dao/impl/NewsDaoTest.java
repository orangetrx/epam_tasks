package com.epam.newsmanager.dao.impl;

import com.epam.newsmanager.bean.Author;
import com.epam.newsmanager.bean.News;
import com.epam.newsmanager.bean.Tag;
import com.epam.newsmanager.bean.transferobject.SearchCriteria;
import com.epam.newsmanager.dao.AuthorDao;
import com.epam.newsmanager.dao.NewsDao;
import com.epam.newsmanager.dao.TagDao;
import com.epam.newsmanager.exception.DaoException;
import com.epam.newsmanager.util.DbTestUtil;
import com.epam.newsmanager.util.PaginationData;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.DatabaseTearDowns;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import javax.sql.DataSource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:test-app-contex.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup(value = {"classpath:com/epam/newsmanager/dao/AuthorDaoTest.xml",
        "classpath:com/epam/newsmanager/dao/NewsDaoTest.xml",
        "classpath:com/epam/newsmanager/dao/TagDaoTest.xml",
        "classpath:com/epam/newsmanager/dao/news_tags_dataset.xml",
        "classpath:com/epam/newsmanager/dao/news_authors_dataset.xml"}, type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDowns(
        {
                @DatabaseTearDown(value = "classpath:com/epam/newsmanager/dao/news_authors_dataset.xml", type = DatabaseOperation.DELETE_ALL),
                @DatabaseTearDown(value = "classpath:com/epam/newsmanager/dao/news_tags_dataset.xml", type = DatabaseOperation.DELETE_ALL)
        }
)
public class NewsDaoTest {

    @Autowired
    private NewsDao newsDao;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private AuthorDao authorDao;

    @Autowired
    private TagDao tagDao;

    @After
    public void reset() {
        DbTestUtil.resetAutoIncrementInNews(dataSource);
        DbTestUtil.resetAutoIncrementInAuthors(dataSource);
        DbTestUtil.resetAutoIncrementInTags(dataSource);
    }

    @Test
    public void testGetAllNews() throws DaoException {
        List<News> actualNews = newsDao.getAllNews();
        int expectedNewsCount = 3;

        Assert.assertEquals(expectedNewsCount, actualNews.size());
    }

    @Test
    public void testGetCountOfAllNews() throws DaoException {
        int expectedCountNews = 3;
        int actualCountNews = newsDao.getCountOfAllNews();
        Assert.assertEquals(expectedCountNews, actualCountNews);
    }

    @Test
    public void testInsertNewsAuthorLink() throws DaoException {
        long dummyNewsId = 2;
        long dummyAuthorId = 1;

        newsDao.insertNewsAuthorLink(dummyAuthorId, dummyNewsId);

        Author actualAuthor = authorDao.getAuthorByNewsId(dummyNewsId);

        Assert.assertEquals(actualAuthor.getAuthorId(), dummyAuthorId);
    }

    @Test
    public void testInsertNewsTagLing() throws DaoException {
        long dummyTagId = 3;
        long dummyNewsId = 1;

        newsDao.insertNewsTagLink(dummyNewsId, dummyTagId);
        List<Tag> actualTagsForNews = tagDao.getTagsForNews(dummyNewsId);
        int expectedCountTagsForNews = 3;

        Assert.assertEquals(actualTagsForNews.size(), expectedCountTagsForNews);
    }

    @Test
    public void testDeleteNewsAuthorLink() throws DaoException {
        long dummyNewsId = 1;
        newsDao.deleteNewsAuthorLink(dummyNewsId);
        Author actualAuthor = authorDao.getAuthorByNewsId(dummyNewsId);

        Assert.assertNull(actualAuthor);
    }

    @Test
    public void testDeleteNewsTagLink() throws DaoException {
        long dummyNewsId = 1;
        newsDao.deleteNewsTagLink(dummyNewsId);
        List<Tag> actualTags = tagDao.getTagsForNews(dummyNewsId);
        int expectedCountTags = 0;
        Assert.assertEquals(expectedCountTags, actualTags.size());
    }

    @Test
    public void testGetNewsByTag() throws DaoException {
        long dummyTagId = 1;
        List<News> actualNews = newsDao.getNewsByTag(dummyTagId);
        int expectedCountOfNews = 1;
        Assert.assertEquals(actualNews.size(), expectedCountOfNews);
    }

    @Test
    public void testGetNewsBySearchCriteria() throws DaoException {
        SearchCriteria dummySearchCriteria = new SearchCriteria();
        dummySearchCriteria.setAuthorId(new Long(2));
        dummySearchCriteria.setTagIdList(new ArrayList<Long>() {{
            add((long) 2);
        }});
        PaginationData paginationData = new PaginationData();
        paginationData.setOffset(0);
        paginationData.setEndset(100);
        List<News> actualNewsList = newsDao.getNewsBySearchCriteria(dummySearchCriteria, paginationData);
        int expectedCountNews = 1;

        Assert.assertEquals(expectedCountNews, actualNewsList.size());
    }

    @Test
    public void testCreate() throws DaoException {

        int countNews = newsDao.getCountOfAllNews();

        News dummyNews = new News();
        dummyNews.setTitle("newstitle");
        dummyNews.setShortText("shorttext");
        dummyNews.setFullText("fulltext");
        dummyNews.setCreationDate(new Date(new Date().getTime()));
        dummyNews.setModificationDate(new Date(new Date().getTime()));

        newsDao.create(dummyNews);

        int actualCountNews = newsDao.getCountOfAllNews();

        Assert.assertEquals(countNews+1, actualCountNews);
    }

    @Test
    public void testRead() throws ParseException, DaoException {
        News dummyNews = new News();
        dummyNews.setNewsId(1);
        dummyNews.setTitle("title1");
        dummyNews.setShortText("short text1");
        dummyNews.setFullText("full text1");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        dummyNews.setCreationDate(simpleDateFormat.parse("2017-02-17 20:20:55"));
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dummyNews.setModificationDate(simpleDateFormat.parse("2017-02-17"));

        News actualNews = newsDao.read((long) 1);

        Assert.assertEquals(actualNews, dummyNews);
    }

    @Test
    public void testDelete() throws DaoException {
        int countNews = newsDao.getCountOfAllNews();
        newsDao.delete(new Long(3));
        int actualCountNews = newsDao.getCountOfAllNews();

        Assert.assertEquals(countNews-1, actualCountNews);
    }

    @Test
    public void testUpdate() throws ParseException, DaoException {
        News dummyNews = new News();
        dummyNews.setNewsId(1);
        dummyNews.setTitle("title1");
        dummyNews.setShortText("short text1");
        dummyNews.setFullText("full text1");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        dummyNews.setCreationDate(simpleDateFormat.parse("2017-02-17 20:20:55"));
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dummyNews.setModificationDate(simpleDateFormat.parse("2017-02-17"));

        newsDao.update(dummyNews);

        News actualNews = newsDao.read((long) 1);

        Assert.assertEquals(actualNews, dummyNews);
    }

}
