package com.epam.newsmanager.service.impl;

import com.epam.newsmanager.bean.Author;
import com.epam.newsmanager.bean.Comment;
import com.epam.newsmanager.bean.News;
import com.epam.newsmanager.bean.Tag;
import com.epam.newsmanager.bean.transferobject.NewsTO;
import com.epam.newsmanager.dao.impl.oracle.OracleAuthorDao;
import com.epam.newsmanager.dao.impl.oracle.OracleCommentDao;
import com.epam.newsmanager.dao.impl.oracle.OracleNewsDao;
import com.epam.newsmanager.dao.impl.oracle.OracleTagDao;
import com.epam.newsmanager.exception.DaoException;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.*;

public class NewsManagementServiceTest {

    @InjectMocks
    private NewsManagementServiceImpl newsManagementService;

    @Mock
    private NewsServiceImpl newsService;

    @Mock
    private AuthorServiceImpl authorService;

    @Mock
    private TagServiceImpl tagService;

    @Mock
    private CommentServiceImpl commentService;

    @Mock
    private OracleNewsDao newsDao;

    @Mock
    private OracleCommentDao commentDao;

    @Mock
    private OracleAuthorDao authorDao;

    @Mock
    private OracleTagDao tagDao;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    //@Test
    public void testViewNewsTO() throws ServiceException, DaoException, NoSuchEntityException {
        NewsTO dummyNewsTo = new NewsTO();
        NewsTO expectedNewsTo;
        Long dummyNewsId = new Long(1);

        News dummyNews = new News();
        dummyNews.setCreationDate(new Date(new Date().getTime()));
        dummyNews.setFullText("dummy full text");
        dummyNews.setNewsId(dummyNewsId);
        dummyNews.setModificationDate(new Date(new Date().getTime()));
        dummyNews.setShortText("dummy short text");
        dummyNews.setTitle("dummy short title");

        Author dummyAuthor = new Author();
        dummyAuthor.setAuthorName("dummy author name");
        dummyAuthor.setAuthorId(1);
        dummyAuthor.setExpired(new Date(new Date().getTime()));

        final Tag dummyTag = new Tag();
        dummyTag.setTagId(1);
        dummyTag.setTagName("dummy tag name");

        final Comment dummyComment = new Comment();
        dummyComment.setCommentId(1);
        dummyComment.setCreationDate(new Date(new Date().getTime()));
        dummyComment.setCommentText("dummy comment text");
        dummyComment.setNewsId(dummyNewsId);

        dummyNewsTo.setAuthor(dummyAuthor);
        dummyNewsTo.setTags(new ArrayList<Tag>() {{
            add(dummyTag);
        }});
        dummyNewsTo.setComments(new ArrayList<Comment>() {{
            add(dummyComment);
        }});
        dummyNewsTo.setNews(dummyNews);

        when(newsManagementService.viewNewsTO(dummyNewsId)).thenReturn(dummyNewsTo);
        expectedNewsTo = newsManagementService.viewNewsTO(dummyNewsId);
        verify(newsDao, atLeastOnce()).read(dummyNewsId);
        assertEquals(expectedNewsTo, dummyNewsTo);
    }

    //@Test
    public void testAddNewsTO() throws ServiceException, DaoException, NoSuchEntityException {
        NewsTO dummyNewsTo = new NewsTO();
        Long dummyNewsId = new Long(1);
        News dummyNews = new News();
        dummyNews.setCreationDate(new Date(new Date().getTime()));
        dummyNews.setFullText("dummy full text");
        dummyNews.setNewsId(dummyNewsId);
        dummyNews.setModificationDate(new Date(new Date().getTime()));
        dummyNews.setShortText("dummy short text");
        dummyNews.setTitle("dummy short title");

        Author dummyAuthor = new Author();
        dummyAuthor.setAuthorName("dummy author name");
        dummyAuthor.setAuthorId(1);
        dummyAuthor.setExpired(new Date(new Date().getTime()));

        final Tag dummyTag = new Tag();
        dummyTag.setTagId(1);
        dummyTag.setTagName("dummy tag name");

        final Comment dummyComment = new Comment();
        dummyComment.setCommentId(1);
        dummyComment.setCreationDate(new Date(new Date().getTime()));
        dummyComment.setCommentText("dummy comment text");
        dummyComment.setNewsId(dummyNewsId);

        dummyNewsTo.setAuthor(dummyAuthor);
        dummyNewsTo.setTags(new ArrayList<Tag>() {{
            add(dummyTag);
        }});
        dummyNewsTo.setComments(new ArrayList<Comment>() {{
            add(dummyComment);
        }});
        dummyNewsTo.setNews(dummyNews);

        when(newsManagementService.addNewsTO(dummyNewsTo)).thenReturn(dummyNewsId);

        Long actualNewsId = newsManagementService.addNewsTO(dummyNewsTo);
        assertEquals(dummyNewsId, actualNewsId);
        verify(newsDao, atLeastOnce()).create(dummyNewsTo.getNews());
    }

    @Test
    public void testViewAllNewsTO() throws ServiceException, NoSuchEntityException {
        List<NewsTO> dummyListOfNewsTO = new ArrayList<>();
        List<NewsTO> expectedNewsTOList;

        when(newsManagementService.viewAllNewsTO()).thenReturn(dummyListOfNewsTO);
        expectedNewsTOList = newsManagementService.viewAllNewsTO();

        assertEquals(expectedNewsTOList, dummyListOfNewsTO);
    }

    @Test
    public void testDeleteNews() throws ServiceException {
        Long dummyNewsId = new Long(1);
        newsManagementService.deleteNews(dummyNewsId);
        verify(newsService, atLeastOnce()).deleteNews(dummyNewsId);
    }
}
