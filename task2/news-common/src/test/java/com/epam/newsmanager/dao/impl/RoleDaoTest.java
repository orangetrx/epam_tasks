package com.epam.newsmanager.dao.impl;

import com.epam.newsmanager.bean.Role;
import com.epam.newsmanager.dao.RoleDao;
import com.epam.newsmanager.exception.DaoException;
import com.epam.newsmanager.util.DbTestUtil;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import javax.sql.DataSource;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:test-app-contex.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup(value = {"classpath:com/epam/newsmanager/dao/UserDaoTest.xml",
                        "classpath:com/epam/newsmanager/dao/RoleDaoTest.xml"}, type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = "classpath:com/epam/newsmanager/dao/RoleDaoTest.xml", type = DatabaseOperation.DELETE_ALL)
public class RoleDaoTest {

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private DataSource dataSource;

    @After
    public void reset() {
        DbTestUtil.resetAutoIncrementInUsers(dataSource);
        DbTestUtil.resetAutoIncrementInRoles(dataSource);
    }

    @Test
    public void testCreate() throws DaoException {
        Role expectedRole = new Role();
        expectedRole.setRoleName("rol");
        expectedRole.setUserId(1);

        long roleId = roleDao.create(expectedRole);

        expectedRole.setRoleId(roleId);
        Role actualRole = roleDao.read(roleId);
        Assert.assertEquals(actualRole, expectedRole);
    }

    @Test
    public void testRead() throws DaoException {
        Role expectedRole = new Role();
        expectedRole.setRoleId(2);
        expectedRole.setUserId(2);
        expectedRole.setRoleName("role2");

        Role actualRole = roleDao.read(new Long(2));

        Assert.assertEquals(actualRole, expectedRole);

    }

    @Test
    public void testGetRolesByUserId() throws DaoException {
        int expectedCountRoles = 1;
        List<Role> actualRoles = roleDao.getRolesByUserId((long) 1);
        Assert.assertEquals(expectedCountRoles, actualRoles.size());
    }

    @Test
    public void testDeleteByUserId() throws DaoException {
        roleDao.deleteByUserId((long) 1);
        List<Role> actualRoles = roleDao.getRolesByUserId((long)1);
        int expectedCountRoles = 0;
        Assert.assertEquals(expectedCountRoles, actualRoles.size());
    }
}
