package com.epam.newsmanager.service.impl;

import com.epam.newsmanager.bean.Role;
import com.epam.newsmanager.dao.impl.oracle.OracleRoleDao;
import com.epam.newsmanager.exception.DaoException;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class RoleServiceTest {

    @InjectMocks
    private RoleServiceImpl roleService;

    @Mock
    private OracleRoleDao roleDao;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAddRole() throws ServiceException, DaoException, NoSuchEntityException {
        long dummyRoleId = 1;
        Role dummyRole = new Role();

        when(roleService.addRole(dummyRole)).thenReturn(dummyRoleId);
        long expectedRoleId = roleService.addRole(dummyRole);

        assertEquals(expectedRoleId, dummyRoleId);
        verify(roleDao, atLeastOnce()).create(dummyRole);
    }

    @Test
    public void testLoadRole() throws DaoException, ServiceException, NoSuchEntityException {
        long dummyRoleId = 1;
        Role dummyRole = new Role();
        Role expectedRole;

        when(roleService.loadRole(dummyRoleId)).thenReturn(dummyRole);
        expectedRole = roleService.loadRole(dummyRoleId);

        assertEquals(expectedRole, dummyRole);
        verify(roleDao, atLeastOnce()).read(dummyRoleId);
    }


}
