package com.epam.newsmanager.service.impl;

import com.epam.newsmanager.bean.Comment;
import com.epam.newsmanager.dao.impl.oracle.OracleCommentDao;
import com.epam.newsmanager.exception.DaoException;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class CommentServiceTest {

    @InjectMocks
    private CommentServiceImpl commentService;

    @Mock
    private OracleCommentDao commentDao;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAddComment() throws DaoException, ServiceException, NoSuchEntityException {
        Comment dummyComment = new Comment();
        long dummyCommentId = 1l;

        when(commentService.addComment(dummyComment)).thenReturn(dummyCommentId);
        long actualId = commentService.addComment(dummyComment);

        verify(commentDao, atLeastOnce()).create(dummyComment);
        assertEquals(actualId, dummyCommentId);
    }

    @Test
    public void testDeleteComment() throws ServiceException, DaoException {
        long dummyCommentId = 1;
        commentService.deleteComment(dummyCommentId);
        verify(commentDao, atLeastOnce()).delete(dummyCommentId);
    }

    @Test
    public void testDeleteCommentByNewsId() throws ServiceException, DaoException {
        long dummyNewsId = 1;
        commentService.deleteCommentByNewsId(dummyNewsId);
        verify(commentDao, atLeastOnce()).deleteCommentsByNewsId(dummyNewsId);
    }

    @Test
    public void testLoadComment() throws ServiceException, DaoException, NoSuchEntityException {
        long dummyCommentId = 1;
        Comment dummyComment = new Comment();
        Comment expectedComment;

        when(commentService.loadComment(dummyCommentId)).thenReturn(dummyComment);
        expectedComment = commentService.loadComment(dummyCommentId);

        verify(commentDao, atLeastOnce()).read(dummyCommentId);
        assertEquals(expectedComment, dummyComment);
    }
}
