<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:url value="${requestScope['javax.servlet.forward.request_uri']}" var="currUrl"/>


<div class="col-md-3" id="leftCol">
    <h4>Admin menu</h4>
    <ul class="nav nav-stacked affix-top" id="sidebar">
        <spring:url value="/admin/news/1" var="newsListUrl" htmlEscape="true"/>
        <spring:url value="/admin/news/" var="newsPageTemplate"/>
        <c:choose>
            <c:when test="${fn:startsWith(currUrl, newsPageTemplate)}">
                <li class="active">
                    <a href="${newsListUrl}">News List</a>
                </li>
            </c:when>
            <c:otherwise>
                <li>
                    <a href="${newsListUrl}">News List</a>
                </li>
            </c:otherwise>
        </c:choose>

        <spring:url value="/admin/openAddNews" var="addNewsUrl" htmlEscape="true"/>
        <c:choose>
            <c:when test="${currUrl == addNewsUrl}">
                <li class="active">
                    <a href="${addNewsUrl}">Add News</a>
                </li>
            </c:when>
            <c:otherwise>
                <li>
                    <a href="${addNewsUrl}">Add News</a>
                </li>
            </c:otherwise>
        </c:choose>

        <spring:url value="/admin/selectAuthor/" var="autorSelectTemplate"/>
        <spring:url value="/admin/authors" var="authorsUrl" htmlEscape="true"/>
        <c:choose>
            <c:when test="${(currUrl == authorsUrl) || (fn:startsWith(currUrl, autorSelectTemplate))}">
                <li class="active">
                    <a href="${authorsUrl}">Add/Update Authors</a>
                </li>
            </c:when>
            <c:otherwise>
                <li>
                    <a href="${authorsUrl}">Add/Update Authors</a>
                </li>
            </c:otherwise>
        </c:choose>


        <spring:url value="/admin/selectTag/" var="tagSelectTemplate"/>
        <spring:url value="/admin/tags" var="tagsUrl" htmlEscape="true"/>
        <c:choose>
            <c:when test="${(currUrl == tagsUrl) || (fn:startsWith(currUrl, tagSelectTemplate))}">
                <li class="active">
                    <a href="${tagsUrl}">Add/Update Tags</a>
                </li>
            </c:when>
            <c:otherwise>
                <li>
                    <a href="${tagsUrl}">Add/Update Tags</a>
                </li>
            </c:otherwise>
        </c:choose>
    </ul>

</div>
