<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <header class="navbar navbar-bright navbar-fixed-top" role="banner">
        <div class="container">
            <div class="navbar-header">
                <ul class="nav navbar-bright navbar-nav">
                    <li>
                        <a class="navbar-brand" href="<c:url value="/admin/news/1"/>">News Portal - Administration</a>
                    </li>
                    <li>
                        <a class="navbar-brand text-right" href="<c:url value="/login/logout"/>">Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </header>