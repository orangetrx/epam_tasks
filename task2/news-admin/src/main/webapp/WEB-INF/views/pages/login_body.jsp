<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

    <form:form name='loginForm' id="loginForm" action="../j_spring_security_check" method='POST' cssClass="form-signin">
        <h2 class="form-signin-heading">Please sign in</h2>
        <input type="hidden" name="${_csrf.parameterName}"
               value="${_csrf.token}" />
        <label for="j_username" class="sr-only">Login</label>
        <input type="text" class="form-control" id="j_username" name='j_username' placeholder="Login" autocomplete="on" autofocus="autofocus">
        <label for="j_password" class="sr-only">Password</label>
        <input type="password" id="j_password" name="j_password" class="form-control" placeholder="Password" required>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        <h4 class="text-center text-danger">${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}</h4>
    </form:form>


