<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="col-md-9" id="mainCol">


    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>New author</h3>
        </div>
        <div class="panel-body">
            <form:form action="/admin/addAuthor" cssClass="form-inline" modelAttribute="author" method="post">
                <label for="newAuthor">Author name</label>
                <form:input id="newAuthor" cssClass="form-control" path="authorName"/>
                <input class="btn btn-primary" type="submit" value="save">
                <form:errors path="authorName" cssClass="text-danger"/>
            </form:form>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h2>
                <small>Authors</small>
            </h2>
        </div>
        <div class="panel-body">
            <ul class="list-group">
                <c:forEach var="authorItem" items="${authors}">
                    <li class="list-group-item authorItem">
                        <form:form action="/admin/updateAuthor" cssClass="form-inline" modelAttribute="updatingAuthor"
                                   method="post">
                            <label for="Author">Author:</label>
                            <c:choose>
                                <c:when test="${authorItem.authorId == updatingAuthor.authorId}">
                                    <form:hidden path="authorId"/>
                                    <form:hidden path="version"/>
                                    <form:input cssClass="form-control" id="Author" disabled="false" path="authorName"/>
                                    <button name="update" class="btn-primary" type="submit">update</button>
                                    <c:if test="${not empty authorItem.expired}">
                                        <button name="expiredFlag" class="btn-danger" type="submit">expire</button>
                                    </c:if>
                                    <a href="<c:url value="/admin/authors"/>" class="btn btn-link">cancel</a>
                                    <form:errors path="authorName" cssClass="text-danger"/>
                                    <c:if test="${not empty message}">
                                        <p class="text-danger">${message}</p>
                                    </c:if>
                                </c:when>
                                <c:otherwise>
                                    <form:input cssClass="form-control" disabled="true" path="authorName"
                                                value="${authorItem.authorName}"/>
                                    <a href="<c:url value="/admin/selectAuthor/${authorItem.authorId}"/>"
                                       class="btn btn-link">edit</a>
                                </c:otherwise>
                            </c:choose>
                        </form:form>
                    </li>
                </c:forEach>
            </ul>

        </div>
    </div>

</div>