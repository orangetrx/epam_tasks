package com.epam.newsmanager.controller;

import com.epam.newsmanager.bean.Author;
import com.epam.newsmanager.controller.constant.RequestParameterName;
import com.epam.newsmanager.controller.constant.TemplateName;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
import com.epam.newsmanager.service.AuthorService;
import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.persistence.OptimisticLockException;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;

@Controller
public class AuthorController {

    @Autowired
    private AuthorService authorService;

    @RequestMapping("admin/authors")
    public ModelAndView openAuthorPage(@ModelAttribute(RequestParameterName.AUTHORS) ArrayList<Author> authors,
                                       @ModelAttribute(RequestParameterName.AUTHOR) Author author,
                                       ModelMap modelMap) {
        ModelAndView modelAndView = new ModelAndView(TemplateName.AUTHORS_TEMPALTE);
        try {
            authors = new ArrayList<>(authorService.loadAllAuthors());
        } catch (ServiceException e) {
            e.printStackTrace();
        }

        if (author == null) {
            author = new Author();
        }
        modelAndView.addObject(BindingResult.MODEL_KEY_PREFIX + RequestParameterName.AUTHOR, modelMap.get(RequestParameterName.ERROR));
        modelAndView.addObject(RequestParameterName.AUTHORS, authors);
        modelAndView.addObject(RequestParameterName.AUTHOR, author);
        modelAndView.addObject(RequestParameterName.UPDATING_AUTHOR, new Author());

        return modelAndView;
    }

    @RequestMapping(value = "/admin/addAuthor", method = RequestMethod.POST)
    public String addAuthor(@ModelAttribute(RequestParameterName.AUTHOR) @Valid Author author,
                            BindingResult bindingResult,
                            RedirectAttributes attributes,
                            HttpSession session) throws ServiceException, NoSuchEntityException {

        if (bindingResult.hasErrors()) {
            attributes.addFlashAttribute(RequestParameterName.ERROR, bindingResult);
            attributes.addFlashAttribute(RequestParameterName.AUTHOR, author);
            return "redirect:/admin/authors/";
        }

        try {
            authorService.addAuthor(author);
        } catch (ServiceException | NoSuchEntityException e) {
            throw e;
        }

        return "redirect:/admin/authors/";
    }

    @RequestMapping(value = "/admin/selectAuthor/{authorId}", method = RequestMethod.GET)
    public ModelAndView selectAuthorForEdit(@PathVariable Long authorId,
                                            @ModelAttribute(RequestParameterName.AUTHORS) ArrayList<Author> authors,
                                            @ModelAttribute(RequestParameterName.UPDATING_AUTHOR) Author author,
                                            @ModelAttribute(RequestParameterName.LOCKED_AUTHOR_MESSAGE) String message,
                                            ModelMap modelMap) throws ServiceException, NoSuchEntityException {
        ModelAndView modelAndView = new ModelAndView(TemplateName.AUTHORS_TEMPALTE);
        try {
            authors = new ArrayList<>(authorService.loadAllAuthors());
            author = authorService.loadAuthor(authorId);
        } catch (ServiceException | NoSuchEntityException e) {
            throw e;
        }
        if (message != null) {
            modelAndView.addObject(RequestParameterName.LOCKED_AUTHOR_MESSAGE, message);
        }
        modelAndView.addObject(BindingResult.MODEL_KEY_PREFIX + RequestParameterName.UPDATING_AUTHOR, modelMap.get(RequestParameterName.ERROR));
        modelAndView.addObject(RequestParameterName.AUTHORS, authors);
        modelAndView.addObject(RequestParameterName.UPDATING_AUTHOR, author);
        modelAndView.addObject(RequestParameterName.AUTHOR, new Author());

        return modelAndView;
    }

    @RequestMapping(value = "/admin/updateAuthor", method = RequestMethod.POST, params = RequestParameterName.UPDATE)
    public String updateAuthor(@ModelAttribute(RequestParameterName.UPDATING_AUTHOR) @Valid Author author,
                               BindingResult bindingResult,
                               RedirectAttributes attributes,
                               HttpSession session) throws ServiceException {

        if (bindingResult.hasErrors()) {
            attributes.addFlashAttribute(RequestParameterName.ERROR, bindingResult);
            attributes.addFlashAttribute(RequestParameterName.UPDATING_AUTHOR, author);
            return "redirect:/admin/selectAuthor/" + author.getAuthorId();
        }

        try {
            authorService.updateAuthor(author);
        } catch (ServiceException e) {
            if (e.getException() instanceof OptimisticLockException || e.getException() instanceof StaleObjectStateException)
            {
                attributes.addFlashAttribute(RequestParameterName.MESSAGE, RequestParameterName.LOCKED_AUTHOR_MESSAGE);
                return "redirect:/admin/selectAuthor/" + author.getAuthorId();
            }
            else {
                throw e;
            }
        }

        return "redirect:/admin/authors/";
    }

    @RequestMapping(value = "/admin/updateAuthor", method = RequestMethod.POST, params = RequestParameterName.EXPIRED_FLAG)
    public String setExpiredAuthor(@ModelAttribute(RequestParameterName.UPDATING_AUTHOR) Author author, BindingResult bindingResult) {
        try {
            authorService.setAuthorExpired(author);
        } catch (ServiceException e) {
            e.printStackTrace();
        }

        return "redirect:/admin/authors/";
    }



}
