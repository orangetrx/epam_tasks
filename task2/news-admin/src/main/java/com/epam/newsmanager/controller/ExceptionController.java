package com.epam.newsmanager.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(Exception.class)
    public ModelAndView handleNoSuchEntityException(Exception e) {
        ModelAndView modelAndView = new ModelAndView("exceptionTemplate");
        modelAndView.addObject("exMess", e.getMessage());
        e.printStackTrace();

        return modelAndView;
    }

}
