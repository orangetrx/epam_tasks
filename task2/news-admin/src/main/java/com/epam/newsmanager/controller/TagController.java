package com.epam.newsmanager.controller;

import com.epam.newsmanager.bean.Tag;
import com.epam.newsmanager.controller.constant.RequestParameterName;
import com.epam.newsmanager.controller.constant.TemplateName;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
import com.epam.newsmanager.service.TagService;
import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.persistence.OptimisticLockException;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;

@Controller
public class TagController {

    @Autowired
    private TagService tagService;

    @RequestMapping("admin/tags")
    public ModelAndView openTagsPage(@ModelAttribute(RequestParameterName.TAGS)ArrayList<Tag> tags,
                                     @ModelAttribute(RequestParameterName.TAG)Tag tag,
                                     ModelMap modelMap) throws ServiceException {
        ModelAndView modelAndView = new ModelAndView(TemplateName.TAG_TEMPLATE);

        try {
            tags = new ArrayList<>(tagService.getAllTags());
        } catch (ServiceException e) {
            throw e;
        }
        if (tag == null) {
            tag = new Tag();
        }
        modelAndView.addObject(BindingResult.MODEL_KEY_PREFIX + RequestParameterName.TAG, modelMap.get(RequestParameterName.ERROR));
        modelAndView.addObject(RequestParameterName.TAGS, tags);
        modelAndView.addObject(RequestParameterName.TAG, tag);
        modelAndView.addObject(RequestParameterName.UPDATING_TAG, new Tag());

        return modelAndView;
    }

    @RequestMapping(value = "admin/addTag", method = RequestMethod.POST)
    public String addTag(@ModelAttribute(RequestParameterName.TAG) @Valid Tag tag,
                         BindingResult bindingResult,
                         RedirectAttributes attributes,
                         HttpSession session) throws ServiceException {

        if (bindingResult.hasErrors()) {
            attributes.addFlashAttribute(RequestParameterName.ERROR, bindingResult);
            attributes.addFlashAttribute(RequestParameterName.TAG, tag);
            return "redirect:/admin/tags/";
        }

        try {
            tagService.addTag(tag);
        } catch (ServiceException e) {
            throw e;
        }

        return "redirect:/admin/tags/";
    }

    @RequestMapping(value = "/admin/selectTag/{tagId}", method = RequestMethod.GET)
    public ModelAndView selectTagForEdit(@PathVariable Long tagId,
                                         @ModelAttribute(RequestParameterName.TAGS) ArrayList<Tag> tags,
                                         @ModelAttribute(RequestParameterName.UPDATING_TAG) Tag tag,
                                         @ModelAttribute(RequestParameterName.MESSAGE) String message,
                                         ModelMap modelMap) throws ServiceException, NoSuchEntityException {
        ModelAndView modelAndView = new ModelAndView(TemplateName.TAG_TEMPLATE);

        try {
            tags = new ArrayList<>(tagService.getAllTags());

            tag = tagService.getTagById(tagId);

        } catch (ServiceException | NoSuchEntityException e) {
            throw e;
        }

        if (message != null) {
            modelAndView.addObject(RequestParameterName.MESSAGE, message);
        }

        modelAndView.addObject(BindingResult.MODEL_KEY_PREFIX + RequestParameterName.UPDATING_TAG,
                modelMap.get(RequestParameterName.ERROR));
        modelAndView.addObject(RequestParameterName.TAGS, tags);
        modelAndView.addObject(RequestParameterName.UPDATING_TAG, tag);
        modelAndView.addObject(RequestParameterName.TAG, new Tag());

        return modelAndView;
    }

    @RequestMapping(value = "/admin/updateTag", method = RequestMethod.POST, params = RequestParameterName.UPDATE)
    public String updateTag(@ModelAttribute(RequestParameterName.UPDATING_TAG) @Valid Tag tag,
                            BindingResult bindingResult,
                            RedirectAttributes attributes,
                            HttpSession session) throws ServiceException {
        if (bindingResult.hasErrors()) {
            attributes.addFlashAttribute(RequestParameterName.ERROR, bindingResult);
            attributes.addFlashAttribute(RequestParameterName.UPDATING_TAG, tag);
            return "redirect:/admin/selectTag/" + tag.getTagId();
        }
        try {
            tagService.updateTag(tag);
        } catch (ServiceException e) {
            if (e.getException() instanceof OptimisticLockException || e.getException() instanceof StaleObjectStateException) {
                attributes.addFlashAttribute(RequestParameterName.MESSAGE, RequestParameterName.LOCKED_TAG_MESSAGE);
                return "redirect:/admin/selectTag/" + tag.getTagId();
            }
            else {
                throw e;
            }
        }
        return "redirect:/admin/tags/";
    }

    @RequestMapping(value = "/admin/updateTag", method = RequestMethod.POST, params = RequestParameterName.DELETE)
    public String deleteTag(@ModelAttribute(RequestParameterName.UPDATING_TAG) Tag tag) throws ServiceException {

        try {
            tagService.deleteTag(tag);
        } catch (ServiceException e) {
            throw e;
        }

        return "redirect:/admin/tags/";
    }
}
