package com.epam.newsmanager.controller.constant;

public final class TemplateName {
    private TemplateName() {}

    public static final String MAIN_TEMPLATE = "mainTemplate";
    public static final String LOGIN_TEMPLATE = "loginTemplate";
    public static final String ADMIN_TEMPLATE = "adminTemplate";
    public static final String ADD_EDIT_NEWS_TEMPLATE = "addEditNewsTemplate";
    public static final String TAG_TEMPLATE = "tagsTemplate";
    public static final String AUTHORS_TEMPALTE = "authorsTemplate";
    public static final String NEWS_TEMPLATE = "newsTemplate";
}
