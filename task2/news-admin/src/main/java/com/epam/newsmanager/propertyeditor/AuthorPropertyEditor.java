package com.epam.newsmanager.propertyeditor;

import com.epam.newsmanager.bean.Author;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
import com.epam.newsmanager.service.AuthorService;

import java.beans.PropertyEditorSupport;

public class AuthorPropertyEditor extends PropertyEditorSupport {


    public AuthorPropertyEditor(Object source) {
        this.authorService = (AuthorService) source;
    }

    private AuthorService authorService;

    @Override
    public String getAsText() {
        Author author = (Author) getValue();
        return Long.toString(author.getAuthorId());
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        try {
            Author author = authorService.loadAuthor(Long.parseLong(text));
            setValue(author);
        } catch (ServiceException | NoSuchEntityException e) {
            e.printStackTrace();
        }
    }
}
