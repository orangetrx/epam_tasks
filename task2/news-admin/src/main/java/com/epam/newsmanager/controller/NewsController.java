package com.epam.newsmanager.controller;

import com.epam.newsmanager.bean.Author;
import com.epam.newsmanager.bean.News;
import com.epam.newsmanager.bean.Tag;
import com.epam.newsmanager.bean.transferobject.NewsIdList;
import com.epam.newsmanager.bean.transferobject.NewsTO;
import com.epam.newsmanager.bean.transferobject.SearchCriteria;
import com.epam.newsmanager.controller.constant.RequestParameterName;
import com.epam.newsmanager.controller.constant.TemplateName;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
import com.epam.newsmanager.pagination.PaginationDataHandler;
import com.epam.newsmanager.service.AuthorService;
import com.epam.newsmanager.service.NewsManagementService;
import com.epam.newsmanager.service.NewsService;
import com.epam.newsmanager.service.TagService;
import com.epam.newsmanager.util.PaginationData;
import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.persistence.OptimisticLockException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class NewsController {

    @Autowired
    private AuthorService authorService;

    @Autowired
    private TagService tagService;

    @Autowired
    private NewsService newsService;

    @Autowired
    private NewsManagementService newsManagementService;


    @RequestMapping("/admin/adminPage")
    public ModelAndView getAdminPage(@ModelAttribute(RequestParameterName.NEWS_TO_LIST) ArrayList<NewsTO> newsTOList,
                                     @ModelAttribute(RequestParameterName.AUTHORS) ArrayList<Author> authors,
                                     @ModelAttribute(RequestParameterName.TAGS) ArrayList<Tag> tags) throws ServiceException, NoSuchEntityException {
        ModelAndView modelAndView = new ModelAndView(TemplateName.ADMIN_TEMPLATE);
        try {
            newsTOList = (ArrayList<NewsTO>) newsManagementService.viewAllNewsTO();
            tags = (ArrayList<Tag>) tagService.getAllTags();
            authors = (ArrayList<Author>) authorService.loadAllAuthors();

            modelAndView.addObject(RequestParameterName.NEWS_TO_LIST, newsTOList);
            modelAndView.addObject(RequestParameterName.AUTHORS, authors);
            modelAndView.addObject(RequestParameterName.TAGS, tags);
            modelAndView.addObject(RequestParameterName.SEARCH_CRITERIA, new SearchCriteria());

            modelAndView.addObject(RequestParameterName.LIST_NEWS_ID, new NewsIdList(newsTOList.size()));

        } catch (ServiceException | NoSuchEntityException e) {
            throw e;
        }
        return modelAndView;
    }

    @RequestMapping("/admin/news/{pageNumber}")
    public ModelAndView getNewsPage(@PathVariable int pageNumber,
                                    @ModelAttribute(RequestParameterName.NEWS_TO_LIST) ArrayList<NewsTO> newsTOList,
                                    @ModelAttribute(RequestParameterName.AUTHORS) ArrayList<Author> authors,
                                    @ModelAttribute(RequestParameterName.TAGS) ArrayList<Tag> tags,
                                    HttpServletRequest request) throws ServiceException, NoSuchEntityException {
        ModelAndView modelAndView = new ModelAndView(TemplateName.ADMIN_TEMPLATE);

        HttpSession session = request.getSession();
        if (session != null) {
            session.removeAttribute(RequestParameterName.SEARCH_CRITERIA);
        }

        try {
            PaginationData paginationData = PaginationDataHandler.formPaginationData(pageNumber);
            newsTOList = new ArrayList<>(newsManagementService.viewNewsPerPage(paginationData));
            tags = new ArrayList<>(tagService.getAllTags());
            authors = new ArrayList<>(authorService.loadAllAuthors());
            Integer countPages = new Double(Math.ceil(newsService.getCountOfNews() * 1.0 / PaginationDataHandler.DEFAULT_RECORDS_PER_PAGE)).intValue();


            modelAndView.addObject(RequestParameterName.COUNT_PAGES, countPages);
            modelAndView.addObject(RequestParameterName.CURRENT_PAGE, pageNumber);

            modelAndView.addObject(RequestParameterName.NEWS_TO_LIST, newsTOList);
            modelAndView.addObject(RequestParameterName.AUTHORS, authors);
            modelAndView.addObject(RequestParameterName.TAGS, tags);
            modelAndView.addObject(RequestParameterName.SEARCH_CRITERIA, new SearchCriteria());

            modelAndView.addObject(RequestParameterName.LIST_NEWS_ID, new NewsIdList(newsTOList.size()));

        } catch (ServiceException | NoSuchEntityException e) {
            throw e;
        }
        return modelAndView;
    }


    @RequestMapping("/admin/search/{pageNumber}")
    public ModelAndView getSearchNews(@PathVariable int pageNumber,
                                      @ModelAttribute(RequestParameterName.SEARCH_CRITERIA) SearchCriteria searchCriteria,
                                      @ModelAttribute(RequestParameterName.NEWS_TO_LIST) ArrayList<NewsTO> newsTOList,
                                      @ModelAttribute(RequestParameterName.AUTHORS) ArrayList<Author> authors,
                                      @ModelAttribute(RequestParameterName.TAGS) ArrayList<Tag> tags,
                                      HttpServletRequest request) throws ServiceException, NoSuchEntityException {
        ModelAndView modelAndView = new ModelAndView(TemplateName.ADMIN_TEMPLATE);

        if (searchCriteria.getTagIdList() == null && searchCriteria.getAuthorId() == null) {
            searchCriteria = (SearchCriteria) request.getSession().getAttribute(RequestParameterName.SEARCH_CRITERIA);
        } else {
            HttpSession session = request.getSession();
            if (session != null) {
                if (session.getAttribute(RequestParameterName.SEARCH_CRITERIA) == null) {
                    session.setAttribute(RequestParameterName.SEARCH_CRITERIA, searchCriteria);
                }
            }
        }
        try {

            PaginationData paginationData = PaginationDataHandler.formPaginationData(pageNumber);
            List<News> newsList = newsService.searchNews(searchCriteria, paginationData);
            newsTOList = new ArrayList<>();
            for (News news : newsList) {
                newsTOList.add(newsManagementService.viewNewsTO(news.getNewsId()));
            }

            Integer countPages = new Double(Math.ceil(newsService.getCountSearchedNews(
                    searchCriteria) * 1.0 / PaginationDataHandler.DEFAULT_RECORDS_PER_PAGE))
                    .intValue();
            modelAndView.addObject(RequestParameterName.COUNT_PAGES, countPages);
            modelAndView.addObject(RequestParameterName.CURRENT_PAGE, pageNumber);

            tags = new ArrayList<>(tagService.getAllTags());
            authors = new ArrayList<>(authorService.loadAllAuthors());
            modelAndView.addObject(RequestParameterName.NEWS_TO_LIST, newsTOList);
            modelAndView.addObject(RequestParameterName.AUTHORS, authors);
            modelAndView.addObject(RequestParameterName.TAGS, tags);
            modelAndView.addObject(RequestParameterName.SEARCH_CRITERIA, searchCriteria);
            modelAndView.addObject(RequestParameterName.LIST_NEWS_ID, new NewsIdList(newsTOList.size()));
        } catch (ServiceException | NoSuchEntityException e) {
            throw e;
        }
        return modelAndView;
    }

    @RequestMapping("/admin/openAddNews")
    public ModelAndView openAddNews(@ModelAttribute(RequestParameterName.NEWS_TO) NewsTO newsTO,
                                    @ModelAttribute(RequestParameterName.AUTHORS) ArrayList<Author> authors,
                                    @ModelAttribute(RequestParameterName.ALL_TAGS) ArrayList<Tag> tags,
                                    ModelMap modelMap) throws ServiceException {
        ModelAndView modelAndView = new ModelAndView(TemplateName.ADD_EDIT_NEWS_TEMPLATE);

        try {
            tags = new ArrayList<>(tagService.getAllTags());
            authors = new ArrayList<>(authorService.loadActualAuthors());
        } catch (ServiceException e) {
            throw e;
        }

        if (newsTO.getNews() == null) {
            newsTO = new NewsTO();
            newsTO.setAuthor(new Author());
            newsTO.setTags(new ArrayList<>());
            newsTO.setNews(new News());
            newsTO.setComments(new ArrayList<>());
        }

        modelAndView.addObject(BindingResult.MODEL_KEY_PREFIX + RequestParameterName.NEWS_TO,
                modelMap.get(RequestParameterName.ERROR));
        modelAndView.addObject(RequestParameterName.NEWS_TO, newsTO);
        modelAndView.addObject(RequestParameterName.AUTHORS, authors);
        modelAndView.addObject(RequestParameterName.ALL_TAGS, tags);

        return modelAndView;
    }

    @RequestMapping(value = "/admin/addNews", method = RequestMethod.POST)
    public String addNews(@ModelAttribute(RequestParameterName.NEWS_TO) @Valid NewsTO newsTO,
                          BindingResult bindingResult,
                          RedirectAttributes attributes,
                          HttpSession session) throws ServiceException, NoSuchEntityException {

        if (bindingResult.hasErrors()) {
            attributes.addFlashAttribute(RequestParameterName.ERROR, bindingResult);
            attributes.addFlashAttribute(RequestParameterName.NEWS_TO, newsTO);
            return "redirect:/admin/openAddNews/";
        }

        try {
            newsManagementService.addNewsTO(newsTO);
        } catch (ServiceException | NoSuchEntityException e) {
            throw e;
        }

        return "redirect:../admin/news/1";
    }

    @RequestMapping(value = "/admin/openEditNews/{newsId}", method = RequestMethod.GET)
    public ModelAndView openEditNews(@PathVariable Long newsId,
                                     @ModelAttribute(RequestParameterName.NEWS_TO) NewsTO newsTO,
                                     @ModelAttribute(RequestParameterName.AUTHORS) ArrayList<Author> authors,
                                     @ModelAttribute(RequestParameterName.ALL_TAGS) ArrayList<Tag> tags,
                                     @ModelAttribute(RequestParameterName.MESSAGE) String message,
                                     ModelMap modelMap) throws ServiceException, NoSuchEntityException {

        ModelAndView modelAndView = new ModelAndView(TemplateName.ADD_EDIT_NEWS_TEMPLATE);
        try {
            newsTO = newsManagementService.viewNewsTO(newsId);
            tags = new ArrayList<>(tagService.getAllTags());
            authors = new ArrayList<>(authorService.loadActualAuthors());
        } catch (ServiceException | NoSuchEntityException e) {
            throw e;
        }

        if (message != null) {
            modelAndView.addObject(RequestParameterName.LOCKED_NEWS_MESSAGE, message);
        }

        modelAndView.addObject(BindingResult.MODEL_KEY_PREFIX + RequestParameterName.NEWS_TO,
                modelMap.get(RequestParameterName.ERROR));
        modelAndView.addObject(RequestParameterName.NEWS_TO, newsTO);
        modelAndView.addObject(RequestParameterName.AUTHORS, authors);
        modelAndView.addObject(RequestParameterName.ALL_TAGS, tags);

        return modelAndView;
    }

    @RequestMapping(value = "/admin/editNews", method = RequestMethod.POST)
    public String editNews(@ModelAttribute(RequestParameterName.NEWS_TO) @Valid NewsTO newsTO,
                           BindingResult bindingResult,
                           RedirectAttributes attributes) throws ServiceException {
        if (bindingResult.hasErrors()) {
            attributes.addFlashAttribute(RequestParameterName.ERROR, bindingResult);
            attributes.addFlashAttribute(RequestParameterName.NEWS_TO, newsTO);
            return "redirect:/admin/openEditNews/" + newsTO.getNews().getNewsId();
        }

        try {
            newsService.editNews(newsTO.getNews());
            newsService.insertNewsAuthorLink(newsTO.getAuthor().getAuthorId(), newsTO.getNews().getNewsId());

            for (Tag tag : newsTO.getTags()) {
                newsService.insertNewsTagLink(newsTO.getNews().getNewsId(), tag.getTagId());
            }
        } catch (ServiceException e) {
            if (e.getException() instanceof OptimisticLockException || e.getException() instanceof StaleObjectStateException) {
                attributes.addFlashAttribute(RequestParameterName.MESSAGE, RequestParameterName.LOCKED_NEWS_MESSAGE);
                return "redirect:/admin/openEditNews/" + newsTO.getNews().getNewsId();
            }
            else {
                throw e;
            }
        }
        return "redirect:../admin/news/1";
    }

    @RequestMapping(value = "/admin/deleteNews", method = RequestMethod.POST)
    public String deleteNews(@ModelAttribute(RequestParameterName.LIST_NEWS_ID) NewsIdList newsIdList,
                             BindingResult result) throws ServiceException {
        for (Long id : newsIdList.getIdList()) {
            try {
                if (id != null) {
                    newsManagementService.deleteNews(id);
                }
            } catch (ServiceException e) {
                throw e;
            }
        }

        return "redirect:../admin/news/1";
    }
}
