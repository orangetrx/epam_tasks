package com.epam.newsmanager.controller;


import com.epam.newsmanager.bean.User;
import com.epam.newsmanager.controller.constant.RequestParameterName;
import com.epam.newsmanager.controller.constant.TemplateName;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
import com.epam.newsmanager.service.UserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.Principal;


@Controller
public class LoginController {


    @Autowired
    private UserService userServiceImpl;

    @RequestMapping(value = "/login/loginTemplate", method = RequestMethod.GET)
    public String login(ModelMap modelMap) {
        return TemplateName.LOGIN_TEMPLATE;
    }

    @RequestMapping(value = "/login/loginPass", method = RequestMethod.GET)
    public String loginPass(ModelMap map, Principal principal) {
        String login = principal.getName();
        //read user from dao
        return "redirect:../admin/news/1";
    }

    @RequestMapping(value = "/login/loginFailed", method = RequestMethod.GET)
    public String loginFailed(Model map, HttpSession session) {
        return "loginTemplate";
    }

    @RequestMapping(value = "/login/logout", method = RequestMethod.GET)
    public String logout(Model map, HttpServletRequest req) {

        req.getSession().invalidate();
        SecurityContextHolder.clearContext();
        return "redirect:loginTemplate";
    }

    @RequestMapping(value = "/check-user", method = RequestMethod.POST)
    public ModelAndView checkUser(@ModelAttribute(RequestParameterName.USER) User user) {
        ModelAndView modelAndView;
        try {
            user.setPassword(DigestUtils.md5Hex(user.getPassword()));
            try {
                user.setUserId(userServiceImpl.addUser(user));
            } catch (NoSuchEntityException e) {
                e.printStackTrace();
            }
            modelAndView = new ModelAndView(TemplateName.MAIN_TEMPLATE);
            modelAndView.addObject(RequestParameterName.USER, user);
        } catch (ServiceException e) {
            modelAndView =  new ModelAndView(TemplateName.LOGIN_TEMPLATE);
            modelAndView.addObject(RequestParameterName.MESSAGE, "Invalid login or password");
        }

        return modelAndView;
    }
}
