package com.epam.newsmanager.propertyeditor;

import com.epam.newsmanager.bean.Tag;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
import com.epam.newsmanager.service.TagService;

import java.beans.PropertyEditorSupport;

public class TagPropertyEditor extends PropertyEditorSupport {

    private TagService tagService;

    public TagPropertyEditor(Object source) {
        this.tagService = (TagService) source;
    }

    @Override
    public String getAsText() {
        return Long.toString(((Tag) getValue()).getTagId());
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        try {
            Tag tag = tagService.getTagById(Long.parseLong(text));
            setValue(tag);
        } catch (ServiceException | NoSuchEntityException e) {
            e.printStackTrace();
        }
    }
}
