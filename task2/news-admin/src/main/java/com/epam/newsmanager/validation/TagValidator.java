package com.epam.newsmanager.validation;


import com.epam.newsmanager.bean.Tag;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class TagValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return Tag.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Tag tag = (Tag) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "tagName", "tagName.required");

        if (tag.getTagName().length() > 30) {
            errors.rejectValue("tagName", "to many symbols", new Object[]{"'tagName'"}, "Many symbols in name");
        }
    }
}
