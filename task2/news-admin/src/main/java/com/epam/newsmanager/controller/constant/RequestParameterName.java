package com.epam.newsmanager.controller.constant;

public final class RequestParameterName {
    private RequestParameterName() {}

    public static final String AUTHOR = "author";
    public static final String AUTHORS = "authors";
    public static final String UPDATING_AUTHOR = "updatingAuthor";

    public static final String ERROR = "error";
    public static final String UPDATE = "update";
    public static final String EXPIRED_FLAG = "expiredFlag";
    public static final String DELETE = "delete";

    public static final String MESSAGE = "message";
    public static final String USER = "user";

    public static final String NEWS_TO = "newsTO";
    public static final String NEWS_TO_LIST = "newsTOList";
    public static final String TAG = "tag";
    public static final String TAGS = "tags";
    public static final String UPDATING_TAG = "updatingTag";
    public static final String SEARCH_CRITERIA = "searchCriteria";
    public static final String LIST_NEWS_ID = "listNewsIds";
    public static final String ALL_TAGS = "allTags";

    public static final String COUNT_PAGES = "countPages";
    public static final String CURRENT_PAGE = "currentPage";

    public static final String COMMENT = "comment";
    public static final String PREV = "prev";
    public static final String NEXT = "next";

    public static final String LOCKED_AUTHOR_MESSAGE = "Current author is locked by another user. Try again later";
    public static final String LOCKED_NEWS_MESSAGE = "Current news is locked by another user. Try again later";
    public static final String LOCKED_TAG_MESSAGE = "Current tag is locked by another user. Try again later";
}
