package com.epam.newsmanager.propertyeditor;

import com.epam.newsmanager.bean.Author;
import com.epam.newsmanager.bean.Tag;
import com.epam.newsmanager.service.AuthorService;
import com.epam.newsmanager.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.support.WebBindingInitializer;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class Initializer implements WebBindingInitializer {

    @Autowired
    private AuthorService authorService;

    @Autowired
    private TagService tagService;

    @InitBinder
    @Override
    public void initBinder(WebDataBinder binder, WebRequest request) {
        binder.registerCustomEditor(Author.class, new AuthorPropertyEditor(authorService));
        binder.registerCustomEditor(Tag.class, new TagPropertyEditor(tagService));
    }
}
