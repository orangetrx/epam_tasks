<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="col-md-9" id="mainCol">

    <ul class="pager">
        <c:choose>
            <c:when test="${not empty searchCriteria}">
                <li>
                    <a href="<c:url value="/controller?command=searchNewsCommand&pageNumber=${pageNumber}"/>">BACK</a>
                </li>
            </c:when>
            <c:otherwise>
                <li>
                    <a href="<c:url value="/controller?command=viewNewsForPageCommand&pageNumber=${pageNumber}"/>">BACK</a>
                </li>
            </c:otherwise>
        </c:choose>
    </ul>

    <div class="jumbotron news">
        <h1>${newsTO.news.title}</h1>

        <p>${newsTO.news.fullText}</p>
    </div>
    <h5>
        <span class="glyphicon glyphicon-time"></span> Post by
                            <span
                                    class="text-primary">${newsTO.author.authorName}
                            </span>,
                        <span class="text-success"><fmt:formatDate pattern="yyyy-MM-dd"
                                                                   value="${newsTO.news.creationDate}"/>
                    </span>
    </h5>
    <h5>
        <c:forEach var="tag" items="${newsTO.tags}">
            <span class="label label-primary">${tag.tagName}</span>
        </c:forEach>
    </h5>
    <ul class="pager">
        <c:if test="${not empty prevNewsId}">
            <li>
                <a href="<c:url value="/controller?command=viewNewsCommand&newsTOId=${prevNewsId}"/>">prev</a>
            </li>
        </c:if>

        <c:if test="${not empty nextNewsId}">
            <li>
                <a href="<c:url value="/controller?command=viewNewsCommand&newsTOId=${nextNewsId}"/>">next</a>
            </li>
        </c:if>
    </ul>
    <hr>
    <form action="controller" class="form-inline" enctype="multipart/form-data" method="post">
        <input type="hidden" name="command" value="addCommentCommand">
        <input type="hidden" name="newsTOId" value="${newsTO.news.newsId}">
        <input type="text" required maxlength="100" class="form-control" name="commentText"/>
        <input type="submit" class="form-control" value="Post comment">
    </form>

    <ul class="list-group">
        <c:forEach var="comment" items="${newsTO.comments}">
            <li class="list-group-item">
                <a href="">
                    <fmt:formatDate type="both"
                                    dateStyle="medium" timeStyle="medium"
                                    value="${comment.creationDate}"/>
                </a>
                <br>

                <div class="row comments">
                    <span>${comment.commentText}</span>
                </div>
            </li>

        </c:forEach>
    </ul>

</div>