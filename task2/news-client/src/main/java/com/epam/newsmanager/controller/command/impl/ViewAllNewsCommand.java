package com.epam.newsmanager.controller.command.impl;

import com.epam.newsmanager.bean.transferobject.NewsTO;
import com.epam.newsmanager.controller.command.Command;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
import com.epam.newsmanager.service.NewsManagementService;
import com.epam.newsmanager.util.constant.JspPageName;
import com.epam.newsmanager.util.constant.RequestParameterName;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Component
@Scope(scopeName = "singleton")
public class ViewAllNewsCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(ViewAllNewsCommand.class.getPackage().getName());

    @Autowired
    NewsManagementService newsManagementService;

    @Override
    public String execute(HttpServletRequest request) {
        try {
            List<NewsTO> newsTOList = newsManagementService.viewAllNewsTO();
            request.setAttribute(RequestParameterName.NEWS_TO_LIST, newsTOList);
        } catch (ServiceException | NoSuchEntityException e) {
            LOGGER.error("Loading all news failed");
        }

        return JspPageName.NEWS_LIST;
    }
}
