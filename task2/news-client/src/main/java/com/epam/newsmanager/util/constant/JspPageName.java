package com.epam.newsmanager.util.constant;

public final class JspPageName {
    public static final String NEWS_LIST = "newsList.jsp";
    public static final String NEWS_PAGE = "newsPage.jsp";
}
