package com.epam.newsmanager.controller.command.impl;

import com.epam.newsmanager.bean.Author;
import com.epam.newsmanager.bean.Tag;
import com.epam.newsmanager.bean.transferobject.NewsTO;
import com.epam.newsmanager.bean.transferobject.SearchCriteria;
import com.epam.newsmanager.controller.command.Command;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
import com.epam.newsmanager.service.AuthorService;
import com.epam.newsmanager.service.NewsManagementService;
import com.epam.newsmanager.service.NewsService;
import com.epam.newsmanager.service.TagService;
import com.epam.newsmanager.util.PaginationData;
import com.epam.newsmanager.util.constant.JspPageName;
import com.epam.newsmanager.util.constant.RequestParameterName;
import com.epam.newsmanager.util.pagination.PaginationDataHandler;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Component
public class ViewNewsForPageCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(ViewNewsForPageCommand.class.getPackage().getName());

    @Autowired
    private NewsManagementService newsManagementService;

    @Autowired
    private NewsService newsService;

    @Autowired
    private TagService tagService;

    @Autowired
    private AuthorService authorService;

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session != null) {
            session.removeAttribute(RequestParameterName.SEARCH_CRITERIA);
        }

        try {
            int pageNumber;
            if (request.getParameter(RequestParameterName.PAGE_NUMBER) == null) {
                pageNumber = PaginationDataHandler.DEFAULT_PAGE_NUMBER;
            }
            else {
                pageNumber = Integer.parseInt(request.getParameter(RequestParameterName.PAGE_NUMBER));
            }
            PaginationData paginationData = PaginationDataHandler.formPaginationData(pageNumber);
            List<NewsTO> newsTOList = newsManagementService.viewNewsPerPage(paginationData);
            List<Tag> tags = tagService.getAllTags();
            List<Author> authors = authorService.loadAllAuthors();
            Integer countPages = new Double(Math.ceil(newsService.getCountOfNews()*1.0 /PaginationDataHandler.DEFAULT_RECORDS_PER_PAGE)).intValue();

            request.setAttribute(RequestParameterName.COUNT_PAGES, countPages);
            request.setAttribute(RequestParameterName.PAGE_NUMBER, pageNumber);
            request.setAttribute(RequestParameterName.NEWS_TO_LIST, newsTOList);
            request.setAttribute(RequestParameterName.TAGS, tags);
            request.setAttribute(RequestParameterName.AUTHORS, authors);
            request.setAttribute(RequestParameterName.SEARCH_CRITERIA, new SearchCriteria());
        } catch (ServiceException | NoSuchEntityException e) {
            LOGGER.error("load news for page error");
        }

        return JspPageName.NEWS_LIST + "?command=viewNewsForPageCommand";
    }
}
